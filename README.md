## integration-lib
Build project:
```
mvn clean install
```

Add integration-lib to pom.xml file of project:
```
<dependency>
    <groupId>nth.lib</groupId>
    <artifactId>common</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>nth.lib</groupId>
    <artifactId>integration</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

Reimport all maven projects

