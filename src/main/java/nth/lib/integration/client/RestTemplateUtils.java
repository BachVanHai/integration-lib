package nth.lib.integration.client;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public class RestTemplateUtils {

  private static Logger logger = LoggerFactory.getLogger(RestTemplateUtils.class);

  @Getter
  @Setter
  private String connectionTimeout;

  int CONNECTION_TIMEOUT_DEFAULT = 50000; //ms

  public ClientHttpRequestFactory getClientHttpRequestFactory() {
    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
        = new HttpComponentsClientHttpRequestFactory();
    clientHttpRequestFactory.setConnectTimeout(
        this.getConnectionTimeout() == null ? CONNECTION_TIMEOUT_DEFAULT
            : Integer.valueOf(this.getConnectionTimeout()));
    return clientHttpRequestFactory;
  }

}
