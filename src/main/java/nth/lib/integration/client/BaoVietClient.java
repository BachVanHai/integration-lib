package nth.lib.integration.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nth.lib.integration.model.AuthorizationResponse;
import nth.lib.integration.model.CompanyConfigDTO;
import nth.lib.integration.model.baoviet.*;
import nth.lib.integration.utils.BaoVietConstant;
import nth.lib.integration.utils.CompanyConfigHelper;
import nth.lib.integration.utils.ObjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class BaoVietClient {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RestTemplate restTemplate;

    public BaoVietClient() {
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
    }

    @Cacheable(value = "bviAccessToken", key = "#username")
    public String getAccessToken(String username, String password, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> body = new HashMap<>();
        body.put("username", username);
        body.put("password", password);
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(body, headers);
        AuthorizationResponse response = restTemplate.postForObject(url, entity, AuthorizationResponse.class);
        return response == null ? null : response.getIdToken();
    }

    public String getAccessToken(CompanyConfigDTO companyConfigDTO) {
        return getAccessToken(
                companyConfigDTO.getUsername(),
                companyConfigDTO.getPassword(),
                companyConfigDTO.getRootUrl() + BaoVietConstant.TOKEN_ENDPOINT);
    }

    public BAFeeModel getBAFee(BAFeeModel feeModel, CompanyConfigDTO companyConfigDTO) {
        HttpHeaders headers = getHeadersWithToken(companyConfigDTO);
        HttpEntity<BAFeeModel> entity = new HttpEntity<>(feeModel, headers);
        log.info("Request to get insurance fee: {}", feeModel);
        BAFeeModel response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + BaoVietConstant.FEE_ENDPOINT,
                entity, BAFeeModel.class);
        log.info("Fee Response: {}", response);
        return response;
    }

    public BAInsuranceClaimInfo createBAOrder(BAInsuranceClaimInfo model, CompanyConfigDTO companyConfigDTO) throws IOException {
        HttpHeaders headers = getHeadersWithToken(companyConfigDTO);
        HttpEntity<BAInsuranceClaimInfo> entity = new HttpEntity<>(model, headers);
        BAInsuranceClaimInfo clone = ObjectUtil.copy(model, BAInsuranceClaimInfo.class);
        List<BAFileContent> files = clone.getInsuranceClaimFiles();
        if (files != null) {
            files.forEach(f -> {
                if (f.getContent() != null) {
                    f.setContent("file attachment");
                }
            });
        }
        List<BAInsured> insuredInfo = clone.getInsuredInfo();
        if (insuredInfo != null) {
            insuredInfo.forEach(i -> {
                if (i.getBirthCertFile() != null) {
                    BAFileContent birthCert = new BAFileContent();
                    birthCert.setContent("birth_cert");
                    i.setBirthCertFile(birthCert);
                }
            });
        }
        List<BAInsured> insuredAllInfo = clone.getInsuredAllInfo();
        if (insuredAllInfo != null) {
            insuredAllInfo.forEach(i -> {
                if (i.getBirthCertFile() != null) {
                    BAFileContent birthCert = new BAFileContent();
                    birthCert.setContent("birth_cert");
                    i.setBirthCertFile(birthCert);
                }
            });
        }
        log.info("Request to create order: {}", new ObjectMapper().writeValueAsString(clone));
        BAInsuranceClaimInfo response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + BaoVietConstant.CREATE_ORDER_ENDPOINT,
                entity, BAInsuranceClaimInfo.class);
        log.info("Order Response: {}", response != null ? response.getCertNumber() : null);
        return response;
    }

    public BAInsuranceClaimInfo getBAOrderInfo(String insuranceClaimNumber, CompanyConfigDTO companyConfigDTO) {
        HttpHeaders headers = getHeadersWithToken(companyConfigDTO);
        Map<String, String> body = new HashMap<>();
        body.put("gycbhNumber", insuranceClaimNumber);
        body.put("id", "");
        body.put("agencyConfirmContent", "");
        log.info("Request to get order info: {}", insuranceClaimNumber);
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(body, headers);
        BAInsuranceClaimInfo response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + BaoVietConstant.ORDER_INFO_ENDPOINT,
                entity, BAInsuranceClaimInfo.class);
        log.info("Order Response: {}", response != null ? response.getStatusPolicy() : null);
        return response;
    }

    public BACert getBACert(String insuranceClaimNumber, CompanyConfigDTO companyConfigDTO, String type) {
        HttpHeaders headers = getHeadersWithToken(companyConfigDTO);
        Map<String, String> body = new HashMap<>();
        body.put("gycbhNumber", insuranceClaimNumber);
        body.put("type", type);
        log.info("Request to get cert: {}", insuranceClaimNumber);
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(body, headers);
        BACert response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + BaoVietConstant.CERT_ENDPOINT,
                entity, BACert.class);
        log.info("Get cert successfully");
        return response;
    }

    public String verifyOtp(String insuranceClaimNumber, CompanyConfigDTO companyConfigDTO, String otp) {
        HttpHeaders headers = getHeadersWithToken(companyConfigDTO);
        Map<String, String> body = new HashMap<>();
        body.put("gycbhNumber", insuranceClaimNumber);
        body.put("otp", otp);
        log.info("Request to verify claims: {}", insuranceClaimNumber);
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(body, headers);
        ResponseEntity<Map<String, String>> response;
        try {
            response = restTemplate.exchange(
                    companyConfigDTO.getRootUrl() + BaoVietConstant.OTP_ENDPOINT,
                    HttpMethod.POST,
                    entity,
                    new ParameterizedTypeReference<Map<String, String>>() {});
            log.info("Claims info response: {}", response.getBody() != null && response.getBody().get("otp") != null ? response.getBody().get("otp") : "");
        } catch (RestClientException e) {
            return "";
        }
        return response.getBody() != null && response.getBody().get("otp") != null ? response.getBody().get("otp") : "";
    }

    public String getDepartmentId(String departmentCode, CompanyConfigHelper configHelper) {
        String departmentConfig = configHelper.getConfig(CompanyConfigHelper.BVI_APARTMENT_CONFIG);
        List<Map<String, String>> configs = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            configs = mapper.readValue(departmentConfig.getBytes(StandardCharsets.UTF_8), new TypeReference<List<Map<String, String>>>() {});
        } catch (Exception e) {
            log.error("Failed to convert json to List<Map<String, String>");
        }
        if (configs != null) {
            return configs.stream().map(i -> i.get(departmentCode)).filter(Objects::nonNull).findFirst().orElse(null);
        }
        return null;
    }

    private HttpHeaders getHeadersWithToken(CompanyConfigDTO companyConfigDTO) {
        String token = getAccessToken(companyConfigDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        return headers;
    }
}
