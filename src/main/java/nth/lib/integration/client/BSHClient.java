package nth.lib.integration.client;

import nth.lib.integration.model.*;
import nth.lib.integration.security.AES128Encrypter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static java.util.Arrays.asList;

@Service
public class BSHClient {

    private static Logger logger = LoggerFactory.getLogger(BSHClient.class);

    public BSHClient() {
    }

    @Cacheable(value = "bshAccessToken", key = "#username")
    public String getAccessToken(String username, String password, String rootUrl) throws Exception {
//        String expried_str = "";
//        if (template.opsForValue().get("bsh_token_expires_in") != null) {
//            template.opsForValue().get("bsh_token_expires_in").toString();
//        }
//        long expired = -1L;
//        try {
//            expired = Long.valueOf(expried_str);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        if (expired > System.currentTimeMillis()) {
            RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
            RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("username", username);
            map.add("password", AES128Encrypter.getInstance().decrypt(password));
            map.add("grant_type", "password");
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            AuthorizationResponse response = restTemplate.postForObject(
                    rootUrl + "/oauth/token", request, AuthorizationResponse.class);
            return response.getAccessToken();
//        } else {
//            return template.opsForValue().get("bsh_access_token").toString();
//        }

    }

    public String getAccessToken(CompanyConfigDTO companyConfigDTO) throws Exception {
        return getAccessToken(companyConfigDTO.getUsername(), companyConfigDTO.getPassword(), companyConfigDTO.getRootUrl());
    }

    public BSHInsuranceInfoResponse addCarInsuranceInfo(BSHCarInsuranceInfo bshInsuranceInfo, CompanyConfigDTO companyConfigDTO) throws Exception {
        logger.debug("addCarInsuranceInfo: {}", bshInsuranceInfo);
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String accessToken = getAccessToken(companyConfigDTO);
        logger.info("BSH Access Token: {}", accessToken);
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<BSHCarInsuranceInfo> request = new HttpEntity<>(bshInsuranceInfo, headers);
        BSHInsuranceInfoResponse response = restTemplate.postForObject(
                companyConfigDTO .getRootUrl() + "/api/mvc/add", request, BSHInsuranceInfoResponse.class);
        return response;

    }

    public BSHInsuranceInfoResponse addMotorInsuranceInfo(BSHMotorInsuranceInfo bshMotorInsuranceInfo, CompanyConfigDTO companyConfigDTO) throws Exception {
        logger.debug("addMotorInsuranceInfo: {}", bshMotorInsuranceInfo);
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String accessToken = getAccessToken(companyConfigDTO);
        logger.info("BSH Access Token: {}", accessToken);
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<BSHMotorInsuranceInfo> request = new HttpEntity<>(bshMotorInsuranceInfo, headers);
        BSHInsuranceInfoResponse response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + "/api/motor/addcert", request, BSHInsuranceInfoResponse.class);
        return response;

    }

    public BSHApproveCertResponse approveCert(BSHApproveCert bshApproveCert, CompanyConfigDTO companyConfigDTO) throws Exception {
        logger.debug("approveCert: {}", bshApproveCert);
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String accessToken = getAccessToken(companyConfigDTO.getUsername(), companyConfigDTO.getPassword(), companyConfigDTO.getRootUrl());
        logger.info("BSH Access Token: {}", accessToken);
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<BSHApproveCert> request = new HttpEntity<>(bshApproveCert, headers);
        BSHApproveCertResponse response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + "/api/mvc/approvecert", request, BSHApproveCertResponse.class);
        return response;

    }

    public BSHApproveCertResponse approveCertMotor(BSHApproveCert bshApproveCert, CompanyConfigDTO companyConfigDTO) throws Exception {
        logger.debug("approveCert: {}", bshApproveCert);
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String accessToken = getAccessToken(companyConfigDTO.getUsername(), companyConfigDTO.getPassword(), companyConfigDTO.getRootUrl());
        logger.info("BSH Access Token: {}", accessToken);
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<BSHApproveCert> request = new HttpEntity<>(bshApproveCert, headers);
        BSHApproveCertResponse response = restTemplate.postForObject(
                companyConfigDTO.getRootUrl() + "/api/motor/approvecert", request, BSHApproveCertResponse.class);
        return response;

    }

    public BSHFileUploadResponse uploadFileToBsh(String username, String password, String rootUrl, String filePath, String ma_dvi, String fType, String so_id) throws Exception {

        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        String accessToken = getAccessToken(username, password, rootUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Bearer " + accessToken);
        MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
        requestMap.add("File", new FileSystemResource(filePath));
        requestMap.add("ma_dvi", ma_dvi);
        requestMap.add("Ftype", fType);
        requestMap.add("so_id", so_id);

        final ParameterizedTypeReference<BSHFileUploadResponse> typeReference = new ParameterizedTypeReference<BSHFileUploadResponse>() {
        };

        final ResponseEntity<BSHFileUploadResponse> exchange = restTemplate.exchange(rootUrl + "/api/file/upload",
                HttpMethod.POST,
                new HttpEntity<>(requestMap, headers), typeReference);
        return exchange.getBody();
    }
}
