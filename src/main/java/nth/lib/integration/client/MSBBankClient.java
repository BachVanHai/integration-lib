package nth.lib.integration.client;

import nth.lib.integration.model.msbBank.MSBBankConfig;
import nth.lib.integration.model.msbBank.MSBBankTransactionReq;
import nth.lib.integration.model.msbBank.MSBBankTransactionRes;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class MSBBankClient {

    RestTemplate restTemplate;

    public MSBBankClient() {
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
    }

    public MSBBankTransactionRes createTransaction(MSBBankTransactionReq msbBankTransactionReq, MSBBankConfig msbBankConfig) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("MERCHANT_ID", msbBankConfig.getMId());
        headers.set("MERCHANT_NAME", msbBankConfig.getMerchantName());
        headers.set("TERMINAL_ID", msbBankConfig.getTid());
        HttpEntity<MSBBankTransactionReq> request = new HttpEntity<>(msbBankTransactionReq, headers);
        MSBBankTransactionRes response = this.restTemplate.postForObject(
                msbBankConfig.getRootUrl() + "api/msb/acq-hub/arcturus/arcturus-process/create-bill", request, MSBBankTransactionRes.class);
        return response;
    }
}
