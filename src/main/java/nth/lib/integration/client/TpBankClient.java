package nth.lib.integration.client;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import nth.lib.integration.model.*;
import nth.lib.integration.model.tpbank.TPBankTransactionReq;
import nth.lib.integration.model.tpbank.TPBankTransactionRes;
import nth.lib.integration.model.tpbank.TpBankConfig;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

@Service
public class TpBankClient {

    private static Logger logger = LoggerFactory.getLogger(TpBankClient.class);

    public TpBankClient() {
    }

//    @Cacheable(value = "tpBankAccessToken", key = "#clientId")
    public String getAccessToken(String clientId, String secretId, String authApiUrl) throws Exception {
        SSLContext sslcontext = SSLContexts.custom()
                .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                .build();

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();
        Unirest.setHttpClient(httpclient);
        String requestBody = String.format("client_id=%s&client_secret=%s&scope=payment&grant_type=client_credentials", clientId, secretId);
        HttpResponse<JsonNode> response = Unirest
                .post(authApiUrl)
                .header("content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .body(requestBody)
                .asJson();
        logger.info("Get TPBank AccessToken request : {}", requestBody);
        logger.info("Get TPBank AccessToken response : {}", response.getBody().toString());
        return response.getBody().getObject().get("access_token").toString();

    }

    private String getAccessToken(TpBankConfig tpBankConfig) throws Exception {
        return getAccessToken(tpBankConfig.getClientId(), tpBankConfig.getSecretId(), tpBankConfig.getRootUrl() + tpBankConfig.getAuthUrl());
    }

    public TPBankTransactionRes createTransaction(TPBankTransactionReq tpBankTransactionReq, TpBankConfig tpBankConfig) throws Exception {
        RestTemplate restTemplate = getRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        String accessToken = getAccessToken(tpBankConfig);
        headers.set("Authorization", "Bearer " + accessToken);
        headers.set("transaction_id", UUID.randomUUID().toString());
        headers.set("api_timestamp", new Timestamp(System.currentTimeMillis()).toString());
        HttpEntity<TPBankTransactionReq> request = new HttpEntity<>(tpBankTransactionReq, headers);
        TPBankTransactionRes response = restTemplate.postForObject(
                tpBankConfig.getRootUrl() + tpBankConfig.getXsaleUrl(), request, TPBankTransactionRes.class);
        return response;
    }

    private static RestTemplate getRestTemplate()
            throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
    }



}
