package nth.lib.integration.client;


import nth.lib.integration.model.mbbank.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;

@Service
public class MBBankClient {

    RestTemplate restTemplate;

    public MBBankClient() {
        RestTemplateUtils restTemplateUtils = new RestTemplateUtils();
        restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
    }

    public MBBankUserInfoRes verifyTokenAndGetUserInfo(String loginToken, MBBankConfig config) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("MERCHANT_CODE", config.getMerchantCode());
        headers.set("MERCHANT_SECRET", config.getMerchantSecret());
        MBBankUserInfoReq mbBankUserInfoReq = new MBBankUserInfoReq(loginToken);
        HttpEntity<MBBankUserInfoReq> request = new HttpEntity<>(mbBankUserInfoReq, headers);
        MBBankUserInfoRes response = this.restTemplate.postForObject(
                config.getRootUrl() + "/api/merchant/v1/session/verify", request, MBBankUserInfoRes.class);
        return response;
    }

    public MBBankTransactionRes createTransaction(MBBankTransactionReq mbBankTransactionReq, MBBankConfig config) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("MERCHANT_CODE", config.getMerchantCode());
        headers.set("MERCHANT_SECRET", config.getMerchantSecret());
        HttpEntity<MBBankTransactionReq> request = new HttpEntity<>(mbBankTransactionReq, headers);
        MBBankTransactionRes response = this.restTemplate.postForObject(
                config.getRootUrl() + "/api/merchant/v1/transaction", request, MBBankTransactionRes.class);
        return response;
    }


}
