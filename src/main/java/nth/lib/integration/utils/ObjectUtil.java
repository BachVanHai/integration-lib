package nth.lib.integration.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;

public class ObjectUtil {
    public static <T extends Serializable> T copy(T input, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(input);
        return mapper.readValue(json, clazz);
    }
}
