package nth.lib.integration.utils;

import java.math.BigDecimal;

public class Const {
    public static BigDecimal DELIVERY_FEE = new BigDecimal(20000);
    public static BigDecimal ATM_FEE = BigDecimal.valueOf(1650);
    public static BigDecimal VISA_MASTER_FEE = BigDecimal.valueOf(2200);;


}
