package nth.lib.integration.utils;

import nth.lib.integration.enumeration.PaymentType;
import nth.lib.integration.enumeration.PrintedCertType;
import nth.lib.integration.model.ContractDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class InsuranceFeeUtils {

    private static final Logger log = LoggerFactory.getLogger(InsuranceFeeUtils.class);

    private static BigDecimal roundUp1000(BigDecimal value) {
        value = value.setScale(0, RoundingMode.FLOOR);
        return value.add(BigDecimal.valueOf(1000).subtract(value.subtract(BigDecimal.valueOf(1000).multiply(value.divide(BigDecimal.valueOf(1000), RoundingMode.DOWN)))));
    }

    public static BigDecimal calculateVnPayFee(BigDecimal insuranceTotalFee, PaymentType paymentType) {
        switch (paymentType) {
            case FUND_TRANSFER:
                return BigDecimal.ZERO;
            case ATM:
            case QR_CODE:
                return roundUp1000(insuranceTotalFee.multiply(BigDecimal.valueOf(0.011)).add(Const.ATM_FEE));
            case VISA_MASTER:
                return roundUp1000(insuranceTotalFee.multiply(BigDecimal.valueOf(0.024)).add(Const.VISA_MASTER_FEE));
        }
        return BigDecimal.ZERO;
    }

    public static BigDecimal calculateAdditionalFee(ContractDTO contractDTO, BigDecimal totalFee) {
        BigDecimal additionalFee = BigDecimal.ZERO;
        if (contractDTO.getPaymentType() != null) {
            BigDecimal payFee = calculateVnPayFee(totalFee, contractDTO.getPaymentType());
            additionalFee = additionalFee.add(payFee);
        }
        if (contractDTO.getPrintedCertType() != null) {
            if (contractDTO.getPrintedCertType() == PrintedCertType.DELIVERY) {
                additionalFee = additionalFee.add(Const.DELIVERY_FEE);
            }
        }
        return additionalFee;
    }
}
