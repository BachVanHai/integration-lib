package nth.lib.integration.utils;

public class BaoVietConstant {
    public static final String TOKEN_ENDPOINT = "/api/agency/account/login";
    public static final String FEE_ENDPOINT = "/api/agency/product/bvg/premium";
    public static final String CREATE_ORDER_ENDPOINT = "/api/agency/product/bvg/createPolicy-Partner";
    public static final String ORDER_INFO_ENDPOINT = "/api/agency/product/agreement/get-productInfo-by-gycbhNumber";
    public static final String CERT_ENDPOINT = "/api/agency/document/download-file-agreement";
    public static final String OTP_ENDPOINT = "/api/agency/product/agreement/check-OTP";
    public static final String PAYMENT_ENDPOINT = "/api/agency/payment/notify-payment-tkct";
    public static final String SURVEY_Q1 = "Trong ba (03) năm qua, có NĐBH nào liệt kê ở trên đã từng đi khám/tư vấn y tế/được chỉ định điều trị/điều trị \n(*) tại phòng khám/bệnh viện/viện điều dưỡng/tố chức y tế hoặc các tổ chức tương tự? \n(*) đi khám/được chỉ định điều trị/điều trị do các bệnh/tình trạng sau có thể không cần kê khai, bao gồm: rối loạn tiêu hóa, ngộ độc thức ăn, cảm lạnh, cúm, sốt xuất huyết, sốt virus (không bao gồm Covid-19), viêm tai/mũi/họng cấp, viêm kết mạc, dị ứng, khám thai, ngừa thai, viêm ruột thừa, sinh con, cạo vôi răng, nhổ răng, viêm lợi, tiêm chủng, chấn thương đã khỏi hoàn toàn và không cần theo dõi hoặc điều trị tiếp.";
    public static final String SURVEY_Q2 = "Có NĐBH nào liệt kê ở trên đang được theo dõi hoặc điều trị thương tật, bệnh hoặc có triệu chứng sức khỏe không ổn định hoặc được chỉ định phải điều trị trong vòng 12 tháng tới không?";
    public static final String SURVEY_Q3 = "Trong ba (03) năm qua, có NĐBH nào liệt kê ở trên đã mắc và/hoặc điều trị một hay nhiều các chứng bệnh sau: viêm hệ thần kinh trung ương (não); Parkinson; thoái hóa khác của hệ thần kinh; mất trí nhớ, hôn mê, bại não, bại liệt; đái tháo đường; suy phổi, tràn khí/dịch phổi, suy hô hấp mãn tính; bệnh mạch máu não, đột quỵ (xuất huyết não/xơ cứng động mạch); suy tim, nhồi máu cơ tim, phẫu thuật tim; viêm gan, xơ gan; suy thận, teo thận, sỏi thận, chạy thận nhân tạo; viêm tụy; ghép tủy; Lupus ban đỏ; lao các loại; phong; u bướu các loại; ung thư các loại; suy tủy; bạch cầu; các bệnh lây qua đường tình dục, hội chứng suy giảm miễn dịch; bệnh bẩm sinh, bệnh di truyền, dị dạng về gen, khuyết tật cơ thể, down?";
    public static final String SURVEY_Q4 = "Có NĐBH nào liệt kê ở trên đã từng được Bảo Việt giải quyết bồi thường theo một Hợp đồng bảo hiếm y tế tương tự; hoặc bị bất kỳ Công ty bảo hiểm nào từ chối bảo hiểm/từ chối tái tục Hợp đồng bảo hiểm y tế tương tự; hoặc được chấp nhận nhưng với điều kiện áp dụng các điều khoản bố sung đặc biệt?";
}
