package nth.lib.integration.utils;

import nth.lib.integration.model.CompanyConfigAdditionalDTO;
import nth.lib.integration.model.CompanyConfigDTO;

import java.util.Arrays;

public class CompanyConfigHelper {

    public static final String CERT_FOLDER = "cert_folder";
    public static final String PREVIEW_CONTEXT_PATH = "portalgateway.previewfile.contextpath";
    public static final String QR_IMAGE_FOLDER = "qrImage_folder";
    public static final String BSH_BASE_URL = "bsh_base_url";
    public static final String BSH_MANAGER_CODE = "bshManagerCode";
    public static final String BSH_WAIVER_LEVEL = "bshWaiverLevel";
    public static final String BSH_CONTENT_INSURANCE = "bshContentInsurance";
    public static final String BSH_INSURANCE_UNIT_CODE_DEFAULT = "bshInsuranceUnitCodeDefault";
    public static final String BSH_EMPLOYEE_INSURANCE_CODE_DEFAULT = "bshEmployeeInsuranceCodeDefault";
    public static final String BSH_INSURANCE_AGENCY_CODE_DEFAULT = "bshInsuranceAgencyCodeDefault";
    public static final String COMPANY_PHONE_NUMBER = "companyPhoneNumber";
    public static final String MANAGER_EMAIL = "managerEmail";
    public static final String BSH_COMPANY_EMAIL = "bshCompanyEmail";
    public static final String BSH_CAR_TNDS_LIABILITY_HUMAN = "bshCarTndsLiabilityHuman";
    public static final String BSH_CAR_TNDS_LIABILITY_ASSETS = "bshCarTndsLiabilityAssets";
    public static final String BSH_MOTOR_TNDS_LIABILITY_HUMAN = "bshMotorTndsLiabilityHuman";
    public static final String BSH_MOTOR_TNDS_LIABILITY_ASSETS = "bshMotorTndsLiabilityAssets";
    public static final String BSH_MANAGER_EMAIL = "bshManagerEmail";
    public static final String BSH_KENH_KT_CS = "bshKenhKTCS";
    public static final String BSH_ADD_CS_CONTRACT = "bshCSAddContractUrl";
    public static final String BSH_APPROVAL_CS_CONTRACT = "bshCSApprovalUrl";
    public static final String BSH_SIGNED_CERT_CS_CONTRACT = "bshCSSignedCertUrl";
    public static final String BSH_CS_MANAGER_APPROVAL = "bshCSManagerApproval";

    public static final String VBI_HASH_SECRET = "vbiHashSecret";
    public static final String VBI_API_CREATE_CAR_CONTRACT = "vbiApiCreateCarContract";
    public static final String VBI_API_CREATE_MOTO_CONTRACT = "vbiApiCreateMotoContract";
    public static final String VBI_CAR_TNDS_LIABILITY_HUMAN = "vbiCarTndsLiabilityHuman";
    public static final String VBI_CAR_TNDS_LIABILITY_ASSETS = "vbiCarTndsLiabilityAssets";
    public static final String VBI_MOTOR_TNDS_LIABILITY_HUMAN = "vbiMotorTndsLiabilityHuman";
    public static final String VBI_MOTOR_TNDS_LIABILITY_ASSETS = "vbiMotorTndsLiabilityAssets";
    public static final String BUY_INSURANCE_ENABLE = "buyInsnsuranceEnable";
    public static final String BUY_INSURANCE_ADDON_ENABLE = "buyInsuranceAddonEnable";
    public static final String BUY_BY_YEAR = "buyByYear";
    public static final String VBI_API_SEARCH_CONTRACT = "vbiApiSearchContract";
    public static final String VBI_API_VIEW_CERTIFIATE = "vbiApiViewCertficate";
    public static final String VBI_HEALTH_CARE_ADVANCE_SECRET = "vbiHealthCareAdvanceSecret";
    public static final String VBI_HEALTH_CARE_ADVANCE_CODE = "vbiHealthCareAdvanceCode";
    public static final String VBI_HEALTH_CARE_ADVANCE_NSD = "vbiHealthCareAdvanceNsd";
    public static final String VBI_HEALTH_CARE_ADVANCE_DOMAIN = "vbiHealthCareAdvanceDomain";
    public static final String VBI_HEALTH_CARE_ADVANCE_POST = "vbiHealthCareAdvancePost";
    public static final String VBI_HEALTH_CARE_ADVANCE_APPROVAL = "vbiHealthCareAdvanceApproval";
    public static final String VBI_HEALTH_CARE_ADVANCE_POLICY = "vbiHealthCareAdvancePolicy";

    public static final String VNI_API_CREATE_TNDS_CONTRACT = "vniApiCreateTndsContract";
    public static final String VNI_API_CREATE_VCX_CONTRACT = "vniApiCreateVcxContract";
    public static final String VNI_API_CREATE_MOTOR_CONTRACT = "vniApiCreateMotorContract";
    public static final String VNI_HASH_SECRET = "vniHashSecret";
    public static final String VNI_CAR_TNDS_LIABILITY_HUMAN = "vniCarTndsLiabilityHuman";
    public static final String VNI_CAR_TNDS_LIABILITY_ASSETS = "vniCarTndsLiabilityAssets";
    public static final String VNI_MOTOR_TNDS_LIABILITY_HUMAN = "vniMotorTndsLiabilityHuman";
    public static final String VNI_MOTOR_TNDS_LIABILITY_ASSETS = "vniMotorTndsLiabilityAssets";
    public static final String VNI_CERT_URL = "vniCertUrl";

    public static final String XTI_LOGIN_URL = "xtiLoginUrl";
    public static final String XTI_API_CREATE_TNDS_CONTRACT = "xtiApiCreateTndsContract";
    public static final String XTI_API_GET_CERT = "xtiApiGetCert";
    public static final String XTI_GRANT_TYPE = "xtiGrantType";
    public static final String XTI_CLIENT_ID = "xtiClientId";
    public static final String XTI_CLIENT_SECRET = "xtiClientSecret";
    public static final String XTI_REDIRECT_URI = "xtiRedirectUri";
    public static final String XTI_CAR_TNDS_LIABILITY_HUMAN = "xtiCarTndsLiabilityHuman";
    public static final String XTI_CAR_TNDS_LIABILITY_ASSETS = "xtiCarTndsLiabilityAssets";
    public static final String XTI_MOTOR_TNDS_LIABILITY_HUMAN = "xtiMotorTndsLiabilityHuman";
    public static final String XTI_MOTOR_TNDS_LIABILITY_ASSETS = "xtiMotorTndsLiabilityAssets";
    public static final String CHANNEL_PTI = "channelPTI";
    public static final String BRANCH_UNIT_PTI = "branchUnitPTI";
    public static final String SINGLE_ENTRY_URL_PTI = "singleEntryUrlPTI";
    public static final String GROUP_ENTRY_URL_PTI = "groupEntryUrlPTI";
    public static final String FIND_DIGITAL_SIGNATURE_FILE = "findDigitalSignatureFile";
    public static final String USER_NAME_SIGNATURE = "uNameSignature";
    public static final String PASS_WORD_SIGNATURE = "pWSignature";
    public static final String SECRET_SIGNATURE = "sCSignature";
    public static final String CHANNEL_CODE_SIGNATURE = "maKenhKySo";
    public static final String NV_PTI = "nvPTI";
    public static final String NV_PTI_CAR = "nvPTICar";
    public static final String URL_GCN = "urlGCN";
    public static final String URL_SIGNATURE = "urlSignature";
    public static final String XTI_API_CREATE_MOTOR_CONTRACT = "xtiApiCreateMotoContract";
    public static final String XTI_API_GET_MOTO_CERT = "xtiApiGetMotoCert";
    public static final String XTI_API_DVHC = "xtiApiDvhc";
    public static final String PTI_MOTOR_TNDS_LIABILITY_HUMAN = "ptiMotorTndsLiabilityHuman";
    public static final String PTI_MOTOR_TNDS_LIABILITY_ASSETS = "ptiMotorTndsLiabilityAssets";
    public static final String PTI_MOTOR_CONNGUOI_LIABILITY = "ptiMotorConNguoiLiability";
    public static final String PTI_SIGNER = "signerPTI";
    public static final String PTI_API_ADD_INFOR_CONTRACT_MOTOR_TNDS = "ptiApiMotorTnds";
    public static final String MANAGER_ID_CONTRACT_BSH ="managerIdContractBSH";
    public static final String MANAGER_EMAIL_CONTRACT_BSH="managerEmailContractBSH";
    public static final String INSURANCE_CODE_BSH_BATG="insuranceCodeBATG";
    public static final String CHANNEL_KT_INON_BSH="kenhKtInOnBSH";
    public static final String PTI_TNDS_HUMAN="ptiCarTndsLiabilityHuman";
    public static final String PTI_TNDS_ASSET="ptiCarTndsLiabilityAssets";
    public static final String PTI_API_ADD_CAR_CONTRACT="ptiCarAddContract";
    public static final String XTI_GET_PRINTED_CERT_NO = "xtiGetPrintedCertNo";
    public static final String PTI_GET_QRCODE = "ptiGetQrCode";
    public static final String PTI_SIGNATURE_FILE_PTI = "signatureFilePTI";
    public static final String PVI_TIME_OUT_CONFIG = "timeOutConfig";
    public static final String LIMIT_DURATION_INSURANCE = "limitDuration";
    public static final String PVI_LOGIN = "pviLogin";
    public static final String PVI_ADD_CERT_MOTOR = "pviAddCertMotor";
    public static final String PVI_GET_CERT = "pviGetCert";
    public static final String PVI_GET_PRICE_MOTOR = "pviGetPriceMotor";
    public static final String PVI_ADD_CERT_CAR = "pviAddCertCar";
    public static final String PVI_GET_PRICE_CAR = "pviGetPriceCar";
    public static final String PVI_ADD_TRAVEL_CONTRACT_URL = "addTravelContractUrl";
    public static final String PVI_GET_SIGNED_CERT = "getSignedCertUrl";
    public static final String PVI_EMAIL_COMPANY = "pviEmailCompany";
    public static final String PVI_ADD_FEE_PP_OVER_70_YO = "addFeePpOvr70Yo";
    public static final String PVI_TRAVEL_LUGGAGE_CODE ="pviTravelLuggageCode";
    public static final String PVI_TRAVEL_PERSON_CODE = "pviTravelPersonCode";
    public static final String PVI_HAPPY_LIFE = "pviHappyLifeEmail";
    public static final String BSH_HAPPY_LIFE = "bshHappyLIfeEmail";
    public static final String VNI_BC_BASE_URL = "vniBCBaseUrl";
    public static final String VNI_BC_CERT_ENDPOINT = "vniBCCertEndpoint";
    public static final String VNI_BC_SOURCE = "vniBCSource";
    public static final String VNI_BC_KEY = "vniBCKey";
    public static final String BVI_HASH_TOKEN = "baoVietHashToken";
    public static final String BVI_PAYMENT_ROOT_URL = "baoVietPaymentUrl";
    public static final String BVI_PROMOTION_CODE = "baovietPromotionCode";
    public static final String BVI_PROMOTION_PROGRAM = "baovietPromotionProgram";
    public static final String BVI_APARTMENT_CONFIG = "baovietDepartmentMapping";

    private CompanyConfigDTO companyConfigDTO;
    public CompanyConfigHelper(CompanyConfigDTO companyConfigDTO) {
        this.companyConfigDTO = companyConfigDTO;
    }

    public String getConfig(String key) {
        for (CompanyConfigAdditionalDTO companyConfigAdditionalDTO :companyConfigDTO.getCompanyConfigAdditionals()) {
            if (companyConfigAdditionalDTO.getKey() != null && companyConfigAdditionalDTO.getKey().equals(key)) {
                return companyConfigAdditionalDTO.getValue();
            }
        }
        return null;
    }

    public CompanyConfigDTO getCompanyConfigDTO() {
        return companyConfigDTO;
    }

    public boolean isInOn() {
        return Arrays.asList("INSURANCE_APP", "ELITE_APP", "TP_BANK_APP").contains(companyConfigDTO.getAppName());
    }

    public void setCompanyConfigDTO(CompanyConfigDTO companyConfigDTO) {
        this.companyConfigDTO = companyConfigDTO;
    }
}
