package nth.lib.integration.utils;
import nth.lib.integration.model.FileUploadResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class FileUploadUtils {
    public static FileUploadResponse uploadFile(String url, Resource resource, String fileInfo, String accessToken) throws IOException {
        URLConnection connection = resource.getURL().openConnection();
        String mimeType = connection.getContentType();
        HttpHeaders filePartHeaders = new HttpHeaders();
        filePartHeaders.setContentType(MediaType.parseMediaType(mimeType));
        filePartHeaders.add(HttpHeaders.CONTENT_DISPOSITION, String.format("form-data; name=\"file\"; filename=\"%s\"", resource.getFilename()));
        HttpEntity filePart = new HttpEntity<>(resource, filePartHeaders);
        HttpHeaders fileInfoHeader = new HttpHeaders();
        fileInfoHeader.setContentType(MediaType.TEXT_PLAIN);
        HttpEntity fileInfoPart = new HttpEntity<>(fileInfo, fileInfoHeader);
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("fileInfo", fileInfoPart);
        map.add("file", filePart);
        nth.lib.integration.client.RestTemplateUtils restTemplateUtils = new nth.lib.integration.client.RestTemplateUtils();
        RestTemplate restTemplate = new RestTemplate(restTemplateUtils.getClientHttpRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        HttpHeaders headers = new HttpHeaders();
        System.out.println(accessToken);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Bearer " + accessToken);
        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(
                map, headers);
        ResponseEntity<FileUploadResponse> result = restTemplate.postForEntity(
                url, requestEntity,
                FileUploadResponse.class);
        return result.getBody();
    }
}
