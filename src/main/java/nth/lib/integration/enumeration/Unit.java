package nth.lib.integration.enumeration;

/**
 * The Unit enumeration.
 */
public enum Unit {
    VND, PERCENT
}
