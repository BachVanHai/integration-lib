package nth.lib.integration.enumeration;

/**
 * The PaymentType enumeration.
 */
public enum PaymentType {
    ATM, VISA_MASTER, QR_CODE, FUND_TRANSFER, DEBT, GPAY, TPBANK, VOUCHER, BONUS, VIMASS, MBBANK
}
