package nth.lib.integration.enumeration;

/**
 * The InsuranceCode enumeration.
 */
public enum InsuranceCode {
    CAR_TNDS, CAR_TNDS_TN,
    CAR_VATCHAT, CAR_CONNGUOI,
    CAR_HANGHOA, MOTOR_TNDS,
    MOTOR_CONNGUOI,VTA,HC,PHC, BC, TA,CS,
    GS, TD, HL, BA
}
