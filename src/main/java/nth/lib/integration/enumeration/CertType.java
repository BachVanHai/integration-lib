package nth.lib.integration.enumeration;

/**
 * The CertType enumeration.
 */
public enum CertType {
    MOTOR, CAR, HEALTH, ELECTRIC_SHOCK, TRAVEL,HOME_SAFETY
}
