package nth.lib.integration.enumeration;

public enum RelationShip {
    GRANDFATHER,
    GRANDMOTHER,
    FATHER,
    MOTHER,
    WIFE,
    HUSBAND,
    CHILD,
    OLDERBROTHER,
    OLDERSISTER,
    BROTHER,
    GRANDCHILDREN,
    OTHER,
    BT,
    BM,
    VC,
    CON,
    ACE,
    DN
}
