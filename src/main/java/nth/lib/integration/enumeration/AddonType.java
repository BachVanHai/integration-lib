package nth.lib.integration.enumeration;

/**
 * The AddonType enumeration.
 */
public enum AddonType {
    NEW, OLD
}
