package nth.lib.integration.enumeration;

/**
 * The WorkFlowType enumeration.
 */
public enum WorkFlowType {
    DELIVERY, INVOICE
}
