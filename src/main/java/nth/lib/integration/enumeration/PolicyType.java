package nth.lib.integration.enumeration;

/**
 * The PolicyType enumeration.
 */
public enum PolicyType {
    EPOLICY, PRINTED_POLICY
}
