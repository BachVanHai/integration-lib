package nth.lib.integration.enumeration;

/**
 * The TaxInvoiceType enumeration.
 */
public enum TaxInvoiceType {
    NONE, OWNER, OTHER_INV, OTHER_ORG
}
