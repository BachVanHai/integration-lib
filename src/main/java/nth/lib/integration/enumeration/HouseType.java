package nth.lib.integration.enumeration;

/**
 * The HouseType enumeration.
 */
public enum HouseType {
    APARTMENT,
    VILLA,
    OR,
    TOWNHOUSE,
    OTHER,
}
