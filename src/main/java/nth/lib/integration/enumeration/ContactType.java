package nth.lib.integration.enumeration;

/**
 * The ContactType enumeration.
 */
public enum ContactType {
    HOME, WORK, OTHER
}
