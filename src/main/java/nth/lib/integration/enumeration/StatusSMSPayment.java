package nth.lib.integration.enumeration;

/**
 * The StatusSMSPayment enumeration.
 */
public enum StatusSMSPayment {
    PENDING,
    WARNING,
    SUCCESS,
}
