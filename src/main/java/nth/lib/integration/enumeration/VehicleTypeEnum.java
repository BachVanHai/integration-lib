package nth.lib.integration.enumeration;

/**
 * The ContractType enumeration.
 */
public enum VehicleTypeEnum {
    CAR, MOTOR
}
