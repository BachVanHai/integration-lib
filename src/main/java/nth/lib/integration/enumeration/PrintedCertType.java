package nth.lib.integration.enumeration;

/**
 * The PrintedCertType enumeration.
 */
public enum PrintedCertType {
    DIRECT, DELIVERY, NONE
}
