package nth.lib.integration.enumeration;

/**
 * The ICType enumeration.
 */
public enum ICType {
    CMND, CCCD, HOCHIEU, MST, KHAC
}
