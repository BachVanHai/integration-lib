package nth.lib.integration.enumeration;

/**
 * The CapacityType enumeration.
 */
public enum CapacityType {
    SEAT, LOAD, NONE, ALL
}
