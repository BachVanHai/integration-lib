package nth.lib.integration.enumeration;

/**
 * The ContractType enumeration.
 */
public enum ContractType {
    CC, MC, HF, TR, FD, ES,HC, VTA, FH, BC, CS, TA, TD, GS,VC,HL, BA
}
