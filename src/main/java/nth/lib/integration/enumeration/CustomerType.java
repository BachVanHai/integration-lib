package nth.lib.integration.enumeration;

/**
 * The CustomerType enumeration.
 */
public enum CustomerType {
    ORG, INV, BANK
}
