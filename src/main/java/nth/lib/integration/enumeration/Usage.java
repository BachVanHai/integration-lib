package nth.lib.integration.enumeration;

/**
 * The Usage enumeration.
 */
public enum Usage {
    KD, KKD, A
}
