package nth.lib.integration.enumeration;

/**
 * The PlaceType enumeration.
 */
public enum PlaceType {
    VIETNAM, IMPORT
}
