package nth.lib.integration.enumeration;


public enum MailTemplate {
    RESET_PASSWORD_EMAIL("reset-password", "(InOn) Thay đổi mật khẩu"),
    CHANGE_PASSWORD("change-password", "(InOn) Thay đổi mật khẩu thành công"),
    ONBOARDING_INON_NEW_REGISTER("onboarding-inon-new-register", "(InOn) Đối tác cần phê duyệt"),
    ONBOARDING_INON_USER_REGISTERED("onboarding-inon-user-registered", "(InOn) Đăng ký đối tác thành công"),
    ONBOARDING_USER_REGISTERED("onboarding-user-registered", "(InOn) Đăng ký đối tác thành công"),
    ONBOARDING_USER_START_COMPLETE_INFO("onboarding-user-start-complete-info", "(InOn) Hoàn thiện thủ tục tạo tài khoản đối tác"),
    ONBOARDING_CHANGE_EMAIL("onboarding-change-email", "(InOn) Thay đổi thông tin email"),
    ONBOARDING_CHANGE_PHONE_NUMBER("onboarding-change-phone-number", "(InOn) Thay đổi thông tin số điện thoại"),
    ONBOARDING_VERIFY_USER("onboarding-verify-user", "(InOn) Xác thực tài khoản"),
    NEW_ACCOUNT_CREATED("new-account-created", "(InOn) Tài khoản tạo mới thành công"),
    USER_INON_NEW_ACCOUNT_CREATED("user-inon-new-account-created", "(InOn) Tài khoản tạo mới thành công"),
    USER_INON_NEW_DEBT_ACCOUNT_CREATED("new-debt-account-created", "(InOn) Cấp hạn mức công nợ thành công"),
    USER_INON_NEW_DEBT_ACCOUNT_CREATED_FOR_KD("new-debt-account-created-for-kd", "(InOn) Cấp hạn mức công nợ đối tác được phê duyệt"),
    USER_INON_NEW_DEBT_ACCOUNT_REJECT_FOR_KD("new-debt-account-reject-for-kd", "(InOn) Cấp hạn mức công nợ đối tác bị từ chối"),
    INON_NEW_SUPPORT_BOOK("inon-new-support-book", "(InOn) Yêu cầu được hỗ trợ."),
    INON_NEW_SUPPORT_BOOK_COMPLETE("inon-new-support-book-complete", "(InOn) Yêu cầu được hỗ trợ hoàn tất."),
    INON_NEW_SUPPORT_BOOK_PROCESS("inon-new-support-book-process", "(InOn) Yêu cầu được hỗ trợ được chỉ định."),
    INON_CANCEL_CONTRACT("cancel-contract","(InOn) Bạn có hợp đồng cần hủy."),
    INON_ADMIN_APPROVAL_CONTRACT("inon-admin-approval-contract","(InOn) Có hợp đồng bị lỗi"),
    INON_CONTRACT_EXPIRED("inon-admin-expired-contract","(InOn) Thông báo hợp đồng sắp hết hiệu lực"),
    INON_GATEWAY_REPORT("gateway-report", "(InOn) Cảnh báo hệ thống"),
    INON_EXPIRED_VEHICLE_CONTRACT("Expired-Insurance-Contract-Vehicle", "(InOn) Thông báo hợp đồng sắp hết hiệu lực bảo hiểm."),
    INON_EXPIRED_PERSONAL_CONTRACT("Expired-Insurance-Contract-Personal", "(InOn) Thông báo hợp đồng sắp hết hiệu lực bảo hiểm."),
    INON_EXPIRED_HOUSE_CONTRACT("Expired-Insurance-Contract-House", "(InOn) Thông báo hợp đồng sắp hết hiệu lực bảo hiểm."),
    INON_REASON_FOR_REJECT("Reject-Insurance-Contract-Vehicle","(InOn) Thông báo về hợp đồng bảo hiểm vật chất xe chưa đủ điều kiện thoả mãn"),
    INON_REJECT_VEHICLE("inon-reject-vehicle-contract","(InOn) Thông báo về yêu cầu hủy bảo hiểm"),
    INON_SEND_CARD_HL_E_INSURANCE("healthcare-card-happy-life-insurance","(InOn) Gửi thẻ bảo hiểm điện tử-Happy Life"),
    TRANSFER_INSURANCE_VEHICLE_TO_SELLER_TPBANK("transfer_Insur_Vehicle_to_seller_TpBank","(InOn) Điều chuyển phụ trách tài sản bảo hiểm"),
    EXPORT_BONUS_HISTORY_FIMI("export_bonus_history_fimi","(InOn) Báo cáo hợp đồng"),
    INON_EXPIRED_CONTRACT_BC("BC-remind-renewal", "(InOn) Thông báo hợp đồng bảo hiểm sức khỏe BestChoice sắp hết hiệu lực bảo hiểm."),
    INON_EXPIRED_CONTRACT_ASSET("TP-remind-asset", "(InOn) Thông báo điều chuyển tài sản đảm bảo"),
    INON_EXPIRED_CONTRACT_ASSET_SEND_TO_SALER("TP-remind-asset-send-to-saler", "(InOn) Thông báo tiếp nhận tài sản");



    private String templateName;
    private String subject;

    MailTemplate(String templateName, String subject) {
        this.templateName = templateName;
        this.subject = subject;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getSubject() {
        return subject;
    }
}
