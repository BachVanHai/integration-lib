package nth.lib.integration.enumeration;

/**
 * The DueDateType enumeration.
 */
public enum DueDateType {
    DAY_30TH, FIFTH_DAY
}
