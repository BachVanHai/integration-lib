package nth.lib.integration.enumeration;

/**
 * The VehicleStatus enumeration.
 */
public enum VehicleStatus {
    NEW, OLD, ROLLOVER
}
