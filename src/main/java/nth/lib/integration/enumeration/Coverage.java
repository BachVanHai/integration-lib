package nth.lib.integration.enumeration;

/**
 * The Coverage enumeration.
 */
public enum Coverage {
    BASIC,
    OVERVIEW,
}
