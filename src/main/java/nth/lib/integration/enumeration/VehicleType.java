package nth.lib.integration.enumeration;

/**
 * The ContractType enumeration.
 */
public enum VehicleType {
    CAR, MOTOR
}
