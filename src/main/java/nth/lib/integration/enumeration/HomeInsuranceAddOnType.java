package nth.lib.integration.enumeration;

/**
 * The InsuranceAddOnType enumeration.
 */
public enum HomeInsuranceAddOnType {
    MATERIAL,
    ASSET,
}
