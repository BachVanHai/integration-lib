package nth.lib.integration.enumeration;

/**
 * The HouseOwnerType enumeration.
 */
public enum HouseOwnerType {
    OWNER,
    OTHER,
}
