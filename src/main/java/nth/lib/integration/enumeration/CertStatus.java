package nth.lib.integration.enumeration;

/**
 * The CertStatus enumeration.
 */
public enum CertStatus {
    USED, AVAILABLE, FAILURE
}
