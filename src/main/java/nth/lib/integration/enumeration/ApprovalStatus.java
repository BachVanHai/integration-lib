package nth.lib.integration.enumeration;

/**
 * The ApprovalStatus enumeration.
 */
public enum ApprovalStatus {
    APPROVED, REJECT, PENDING, TIMEOUT
}
