package nth.lib.integration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nth.lib.integration.enumeration.VehicleType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerInfoBuyInsurance {

    private String fullName;
    private String telephoneNumber;
    private String email;
    private String address;
    private String numberPlate;
    private String vehicleCode;
    private String frameNo;
    private String machineNo;
    private String BHNN;
    private String additionalValue;
}
