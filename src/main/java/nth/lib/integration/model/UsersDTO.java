package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class UsersDTO implements Serializable {

    private Long id;

    private String username;

    private String userCode;

    private String fullName;

    private String phoneNumber;

    private String email;

    private String icNumber;

    private String icType;

    private String groupId;

    private String dateOfBirth;

    private String gender;

    private String createdBy;

    private String updatedBy;

    private Instant createdDate;

    private Instant updatedDate;

    private String refId;

    private String refByUser;

    private String status;

    private Integer level;

    private String userType;

    private String parentId;

    private String managerId;

    private Instant registerDate;

    private Boolean deletionFlag;

    private Boolean client;

    private Long userContactId;

    private UserDetailInfoDTO userDetails;

    private UserSettingsDTO userSettings;

}
