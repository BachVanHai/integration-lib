package nth.lib.integration.model;

import nth.lib.integration.enumeration.PaymentType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

public class VimassTransRequestDTO implements Serializable {

    private Long id;

    private String contractCode;

    private String insuranceType;

    private Long duration;

    private String ownerName;

    private BigDecimal amount;

    private Instant createdDate;

    private String error;

    private String transactionId;

    private String vimassTransResponseId;

    private String contractId;

    private PaymentType paymentType;

    private String idVimass;

    public String getIdVimass() {
        return idVimass;
    }

    public void setIdVimass(String idVimass) {
        this.idVimass = idVimass;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getVimassTransResponseId() {
        return vimassTransResponseId;
    }

    public void setVimassTransResponseId(String vimassTransResponseId) {
        this.vimassTransResponseId = vimassTransResponseId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
