package nth.lib.integration.model;
import lombok.Data;
import nth.lib.integration.enumeration.Gender;
import nth.lib.integration.enumeration.ICType;

import java.io.Serializable;
import java.time.Instant;


@Data
public class PartnerOGDTO implements Serializable {
    
    private Long id;

    private String userCode;

    private String fullName;

    private String phoneNumber;

    private String password;

    private String email;

    private String createBy;

    private String updateBy;

    private Instant createDate;

    private Instant updateDate;

    private String refId;

    private Boolean isDeleted;

    private String icNumber;

    private ICType icType;

    private Instant dateOfBirth;

    private Gender gender;

    private String registerToken;

    private String address;

    private String city;

    private String ward;

    private String district;

    private String bankNumber;

    private String bankName;

    private String bankBranch;

    private String refByUser;

    private String deviceId;


}
