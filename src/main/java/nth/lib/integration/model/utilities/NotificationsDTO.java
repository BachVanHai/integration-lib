package nth.lib.integration.model.utilities;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class NotificationsDTO implements Serializable {

    private Long id;
    private Long userId;
    private Boolean read;
    private Boolean deleted;
    private byte[] content;
    private Instant sendDate;
    private Instant updateDate;
    private String updateBy;
    private Long templateId;
    private Long notificationTemplateHisId;
    private String title;
    private String shortContent;
    private String notificationType;
    private String deviceId;
}
