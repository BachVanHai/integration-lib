package nth.lib.integration.model.utilities;

public class SupportBookEmail {
    private String code;
    private String fullName;
    private String phoneNumber;
    private String email;
    private String title;
    private String content;
    private Long numberOfSubject;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getNumberOfSubject() {
        return numberOfSubject;
    }

    public void setNumberOfSubject(Long numberOfSubject) {
        this.numberOfSubject = numberOfSubject;
    }
}
