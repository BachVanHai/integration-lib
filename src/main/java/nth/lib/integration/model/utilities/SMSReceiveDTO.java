package nth.lib.integration.model.utilities;

import lombok.Data;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.enumeration.StatusSMSPayment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

@Data
public class SMSReceiveDTO{


    private Long id;

    private String contractCode;

    private String phoneNumber;

    private String bank;

    private BigDecimal totalFeeTransferred;

    private String fullMessageText;

    private StatusSMSPayment status;

    private Instant createdDate;

    private String createdBy;

    private Instant updateDate;

    private String updateBy;

}
