package nth.lib.integration.model.utilities;

public class SupportBookSentNotificationsRequest {
    private String userId;
    boolean isMessage;
    private Long numberOfMessage;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isMessage() {
        return isMessage;
    }

    public void setMessage(boolean message) {
        isMessage = message;
    }

    public Long getNumberOfMessage() {
        return numberOfMessage;
    }

    public void setNumberOfMessage(Long numberOfMessage) {
        this.numberOfMessage = numberOfMessage;
    }
}
