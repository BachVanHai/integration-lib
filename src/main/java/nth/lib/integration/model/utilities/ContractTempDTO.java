package nth.lib.integration.model.utilities;

import lombok.Data;

import java.time.Instant;

@Data
public class ContractTempDTO {
    private String customerName;
    private String gender;
    private String phoneNumber;
    private String email;
    private String address;
    private String vehicleType;
    private String manufactureName;
    private String brandName;
    private String numberPlate;
    private String machineNo;
    private String frameNo;
    private Instant createdDate;
    private Instant updatedDate;
    private String createdBy;
    private String updatedBy;
    private String code;
    private String contractDescription;
    private String icNo;
    private String city;
}
