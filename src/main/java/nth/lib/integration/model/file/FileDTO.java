package nth.lib.integration.model.file;

import lombok.Data;

import java.time.Instant;

@Data
public class FileDTO {
    private Long id;

    private String code;

    private String contractId;

    private String docCode;

    private String name;

    private String path;

    private String type;

    private Instant uploadDate;

    private String uploadBy;

    private String userId;

    private String deviceId;

    private String ipServer;

    private String ipClientUpload;

    private String checksum;

    private String docType;

    private Integer version;

    private Instant expiredDate;

    private String downloadFromUrl;

    private String fileValidation;

    private Boolean isEnabled;
}
