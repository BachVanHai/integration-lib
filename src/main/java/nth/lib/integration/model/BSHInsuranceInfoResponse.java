
package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BSHInsuranceInfoResponse {

    @JsonProperty("Status")
    private String status;
    @JsonProperty("ErrCode")
    private String errCode;
    @JsonProperty("Message")
    private String message;

    @JsonProperty("Data")
    private InsuranceData data;

    @Data
    public static class InsuranceData {
        @JsonProperty("soID")
        private String soID;
        @JsonProperty("soID_DT")
        private String soIDDt;

    }

}
