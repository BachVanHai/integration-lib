package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.MailTemplate;

import java.util.List;

@Data
public class EmailRemindAssetTPBank {
    private String tpBankName;
    private String tpBankEmail;
    private List<CustomerInfoTpBankAsset> customerInfoTpBankAssets;
    private String link;
    private Boolean isSaler;

    @Data
    public static class CustomerInfoTpBankAsset {
        String customerName;
        String assetCode;
        String assetType;
        String startDate;
        String expiredDate;
        String status;
    }
}
