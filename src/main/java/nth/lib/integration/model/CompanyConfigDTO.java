package nth.lib.integration.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class CompanyConfigDTO implements Serializable {

    private Long id;

    private String appName;

    private String companyId;

    private String compShortName;

    private String companyName;

    private String kenhKt;

    private String rootUrl;

    private String username;

    private String password;

    private String accessKey;

    private String secrectKey;

    private Instant createdAt;

    private Instant updatedAt;

    private Boolean isEnable;

    private Long order;

    private Set<CompanyConfigAdditionalDTO> companyConfigAdditionals = new HashSet<>();

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompShortName() {
        return compShortName;
    }

    public void setCompShortName(String compShortName) {
        this.compShortName = compShortName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getKenhKt() {
        return kenhKt;
    }

    public void setKenhKt(String kenhKt) {
        this.kenhKt = kenhKt;
    }

    public String getRootUrl() {
        return rootUrl;
    }

    public void setRootUrl(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecrectKey() {
        return secrectKey;
    }

    public void setSecrectKey(String secrectKey) {
        this.secrectKey = secrectKey;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Set<CompanyConfigAdditionalDTO> getCompanyConfigAdditionals() {
        return companyConfigAdditionals;
    }

    public void setCompanyConfigAdditionals(Set<CompanyConfigAdditionalDTO> companyConfigAdditionals) {
        this.companyConfigAdditionals = companyConfigAdditionals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyConfigDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyConfigDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyConfigDTO{" +
                "id=" + getId() +
                ", appName='" + getAppName() + "'" +
                ", companyId='" + getCompanyId() + "'" +
                ", compShortName='" + getCompShortName() + "'" +
                ", companyName='" + getCompanyName() + "'" +
                ", kenhKt='" + getKenhKt() + "'" +
                ", rootUrl='" + getRootUrl() + "'" +
                ", username='" + getUsername() + "'" +
                ", password='" + getPassword() + "'" +
                ", accessKey='" + getAccessKey() + "'" +
                ", secrectKey='" + getSecrectKey() + "'" +
                ", createdAt='" + getCreatedAt() + "'" +
                ", updatedAt='" + getUpdatedAt() + "'" +
                ", isEnable='" + isIsEnable() + "'" +
                "}";
    }
}
