package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class InsuranceListDTO implements Serializable {
    
    private Long id;

    private String code;

    private String name;

    private String keyLang;

    private String insuranceCompanyId;

    private Boolean isEnabled;

    private Boolean isVisible;

    private String parentId;


}
