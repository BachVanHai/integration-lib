package nth.lib.integration.model;


import lombok.Data;

import java.io.Serializable;

@Data
public class UserSettingsDTO implements Serializable {

    private Long id;

    private Boolean authBio;

    private String language;

    private String avatar;

    private String defaultDeviceId;

    private Boolean systemNotify;

    private Boolean promoteNotify;

    private Long userId;


}
