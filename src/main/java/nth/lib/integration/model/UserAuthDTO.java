package nth.lib.integration.model;

import lombok.Data;

import java.time.Instant;

@Data
public class UserAuthDTO {
    private Long id;

    private String username;

    private String password;

    private String userId;

    private Boolean isLocked;

    private String sourceApp;

    private String authMethod;

    private String lastLoginDeviceId;

    private Instant lastLoginSuccess;

    private Instant lastLoginFail;

    private Integer loginFailCount;

    private String token;
}
