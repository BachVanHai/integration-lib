package nth.lib.integration.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CommonResp {
    String code;
    Object data;
    LangMessage message;
    String messageId;
}
