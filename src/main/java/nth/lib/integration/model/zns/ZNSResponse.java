package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZNSResponse {

    @JsonProperty(value = "error")
    private int error;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "data")
    private Data data;

    @lombok.Data
    private static class Data{
        @JsonProperty("msg_id")
        private String msgId;

        @JsonProperty("sent_time")
        private String sentTime;

        @JsonProperty("quota")
        private Quota quota;


        @lombok.Data
        private static class Quota{

            @JsonProperty("dailyQuota")
            private String dailyQuota;

            @JsonProperty("remainingQuota")
            private String remainingQuota;
        }
    }
}
