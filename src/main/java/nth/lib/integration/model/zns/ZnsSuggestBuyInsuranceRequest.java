package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ZnsSuggestBuyInsuranceRequest {
    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "template_id")
    private String templateId;

    @JsonProperty(value = "template_data")
    private TemplateDateSuggestBuyVehicleInsurance templateDateSuggestBuyVehicleInsurance;

    @JsonProperty(value = "tracking_id")
    private String trackingId;

    private Long znsHistoryId;

    @Data
    public static class TemplateDateSuggestBuyVehicleInsurance {
        @JsonProperty("customer_name")
        private String customerName;
        @JsonProperty("phone_number")
        private String phoneNumber;
        @JsonProperty("address")
        private String address;
        @JsonProperty("vehicle_type")
        private String vehicleType;
        @JsonProperty("manufacture_name")
        private String manufactureName;
        @JsonProperty("brand_name")
        private String brandName;
        @JsonProperty("number_plate")
        private String numberPlate;
        @JsonProperty("machine_no")
        private String machineNo;
        @JsonProperty("frame_no")
        private String frameNo;
        @JsonProperty("link")
        private String link;
    }
}
