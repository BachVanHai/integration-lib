package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TndsSimpleRequest {

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "template_id")
    private String templateId;

    @JsonProperty(value = "template_data")
    private TemplateData templateData;

    @JsonProperty(value = "tracking_id")
    private String trackingId;

    private Long znsHistoryId;

    @Data
    public static class TemplateData  {
        @JsonProperty("customer_name")
        private String customerName;

        @JsonProperty("vehicle_brand")
        private String vehicleBrand;

        @JsonProperty("number_plate")
        private String numberPlate;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getVehicleBrand() {
            return vehicleBrand;
        }

        public void setVehicleBrand(String vehicleBrand) {
            this.vehicleBrand = vehicleBrand;
        }

        public String getNumberPlate() {
            return numberPlate;
        }

        public void setNumberPlate(String numberPlate) {
            this.numberPlate = numberPlate;
        }
    }
}
