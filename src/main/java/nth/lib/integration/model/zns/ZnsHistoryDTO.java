package nth.lib.integration.model.zns;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;


public class ZnsHistoryDTO implements Serializable {

    private Long id;

    private String telephoneNumber;

    private String status;

    private Instant sentTime;

    private String templateId;

    private byte[] data;

    private String dataText;

    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getDataText() {
        return dataText;
    }

    public void setDataText(String dataText) {
        this.dataText = dataText;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getSentTime() {
        return sentTime;
    }

    public void setSentTime(Instant sentTime) {
        this.sentTime = sentTime;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ZnsHistoryDTO)) {
            return false;
        }

        ZnsHistoryDTO znsHistoryDTO = (ZnsHistoryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, znsHistoryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ZnsHistoryDTO{" +
            "id=" + getId() +
            ", telephoneNumber='" + getTelephoneNumber() + "'" +
            ", status='" + getStatus() + "'" +
            ", sentTime='" + getSentTime() + "'" +
            ", templateId='" + getTemplateId() + "'" +
            "}";
    }
}
