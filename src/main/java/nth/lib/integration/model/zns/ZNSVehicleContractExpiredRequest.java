package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZNSVehicleContractExpiredRequest {
    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "template_id")
    private String templateId;

    @JsonProperty(value = "template_data")
    private TemplateData templateData;

    @JsonProperty(value = "tracking_id")
    private String trackingId;

    private Long znsHistoryId;

    @Data
    public static class TemplateData  {

        @JsonProperty("customer_name")
        private String customerName;

        @JsonProperty("contract_code")
        private String contractCode;

        @JsonProperty("exprire_date")
        private String exprireDate;

        @JsonProperty("insurance_type")
        private String insuranceType;

        @JsonProperty("insurance_company")
        private String insuranceCompany;

        @JsonProperty("number_plate")
        private String numberPlate;

        @JsonProperty("total_fee")
        private String totalFee;

        @JsonProperty("code")
        private String code;
    }
}
