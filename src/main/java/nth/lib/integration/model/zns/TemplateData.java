package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateData {

    @JsonProperty(value = "order_code")
    private String orderCode;

    @JsonProperty(value = "customer_name")
    private String customerName;

    @JsonProperty(value = "company_name")
    private String companyName;

    @JsonProperty(value = "contract_id")
    private String contractId;

    @JsonProperty(value = "insurance_type")
    private String insuranceType;

    @JsonProperty(value = "file_name")
    private String fileName;

}
