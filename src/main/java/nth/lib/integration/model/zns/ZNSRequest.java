package nth.lib.integration.model.zns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ZNSRequest {

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "template_id")
    private String templateId;

    @JsonProperty(value = "template_data")
    private TemplateData templateData;

    @JsonProperty(value = "tracking_id")
    private String trackingId;

    private Long znsHistoryId;

}
