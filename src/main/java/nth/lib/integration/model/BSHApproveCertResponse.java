
package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BSHApproveCertResponse {

    @JsonProperty("Status")
    private String status;
    @JsonProperty("ErrCode")
    private String errCode;
    @JsonProperty("Data")
    private Data data;

    @lombok.Data
    public static class Data {

        @JsonProperty("url")
        private String url;
        @JsonProperty("soID")
        private String soID;
        @JsonProperty("soHD")
        private String soHD;
        @JsonProperty("Ttrang")
        private String ttrang;
        @JsonProperty("soGCN")
        private String soGCN;

    }

}
