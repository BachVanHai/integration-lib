package nth.lib.integration.model.baoviet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BaoVietPaymentEntity {
    @JsonProperty("gycbhNumber")
    private String insuranceClaimsNumber;
    private String paymentFee;
    private String paymentType;
    private String paymentCode;
    @JsonProperty("soTienBangChu")
    private String amountInWords;
    private String qrCode;
}
