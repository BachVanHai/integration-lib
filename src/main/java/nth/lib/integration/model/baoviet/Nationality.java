package nth.lib.integration.model.baoviet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Nationality {
    @JsonProperty("quocTichCode")
    private String code;
    @JsonProperty("quocTichName")
    public String name;
}
