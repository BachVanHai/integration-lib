package nth.lib.integration.model.baoviet;

import lombok.Data;

@Data
public class ReceiverUserInfo {
    private String address;
    private String email;
    private String mobile;
    private String name;
}
