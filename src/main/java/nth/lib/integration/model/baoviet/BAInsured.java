package nth.lib.integration.model.baoviet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BAInsured {
    @JsonProperty("checkBenhLyNghiemTrong")
    private String checkSeriousIllness;

    private String checkReuse;

    @JsonProperty("chuongTrinhBh")
    private String insuranceProgram;

    private String confirmMethod;

    private String discount;

    @JsonProperty("gksFile")
    private BAFileContent birthCertFile;

    @JsonProperty("gycbhId")
    private String insuranceClaimId;

    @JsonProperty("hasExtracare")
    private boolean hasExtraCare;

    @JsonProperty("hasNguoithuhuong")
    private boolean hasBeneficiary;

    private String isShowPersonList;

    @JsonProperty("ketqua")
    private String result;

    private String loading;

    @JsonProperty("ngoaitruChk")
    private String outpatientOption;

    @JsonProperty("ngoaitruPhi")
    private String outpatientFee;

    @JsonProperty("nguoidbhCmnd")
    private String insuredIcNo;

    @JsonProperty("nguoidbhDiachi")
    private String insuredAddress;

    @JsonProperty("nguoidbhDienthoai")
    private String insuredPhoneNumber;

    @JsonProperty("nguoidbhEmail")
    private String insuredEmail;

    @JsonProperty("nguoidbhGioitinh")
    private String insuredGender;

    @JsonProperty("nguoidbhName")
    private String insuredName;

    @JsonProperty("nguoidbhNgaysinh")
    private String insuredDob;

    @JsonProperty("nguoidbhQuanhe")
    private String relateOfInsuredAndApplicant;

    @JsonProperty("nguoithCmnd")
    private String beneficiaryIcNo;

    @JsonProperty("nguoithDiachi")
    private String beneficiaryAddress;

    @JsonProperty("nguoithDienthoai")
    private String beneficiaryPhoneNumber;

    @JsonProperty("nguoithEmail")
    private String beneficiaryEmail;

    @JsonProperty("nguoithName")
    private String beneficiaryName;

    @JsonProperty("nguoithNgaysinh")
    private String beneficiaryDob;

    @JsonProperty("nguoithQuanhe")
    private String relateOfInsuredAndBeneficiary;

    @JsonProperty("nhakhoaChk")
    private String dentalOption;

    @JsonProperty("nhakhoaPhi")
    private String dentalFee;

    @JsonProperty("oldGCN")
    private String oldCert;

    private String oldPolicyNumber;

    private String percentID;

    private String personOrder;

    @JsonProperty("phuPhiBh")
    private String loadingFee;

    private String policyParent;

    @JsonProperty("qlChinhPhi")
    private String mainFee;

    @JsonProperty("showGiamDinh")
    private String showAssessment;

    @JsonProperty("quocTich")
    private Nationality nationality;

    @JsonProperty("quocTichCode")
    private String nationalityCode;

    private String riskAssessmentContent;

    private String riskAssessmentStatus;

    private String serial;

    @JsonProperty("smcnChk")
    private String lifeOption;

    @JsonProperty("smcnPhi")
    private String lifeFee;

    @JsonProperty("smcnSotienbh")
    private String lifeValue;

    @JsonProperty("taiTuc")
    private String renewal;

    @JsonProperty("tangGiamPhanTram")
    private String adjustPercent;

    @JsonProperty("tanggiamPhi")
    private String adjustFee;

    @JsonProperty("tanggiamPhiNoidung")
    private String adjustContent;

    @JsonProperty("thaisanChk")
    private String maternityOption;

    @JsonProperty("thaisanPhi")
    private String maternityFee;

    @JsonProperty("tncnChk")
    private String accidentOption;

    @JsonProperty("tncnPhi")
    private String accidentFee;

    @JsonProperty("tncnSotienbh")
    private String accidentValue;

    @JsonProperty("tongPhiBH")
    private String totalFee;

    @JsonProperty("tongPhiSauTangGiam")
    private String totalFeeAfterAdjust;

    @JsonProperty("ttskCheck")
    private String checkHealthInfo;

    @JsonProperty("tuoi")
    private String age;

    @JsonProperty("tuoiNDBH")
    private String insuredAge;

    @JsonProperty("tyLeBoiThuong")
    private String compensationRate;
}
