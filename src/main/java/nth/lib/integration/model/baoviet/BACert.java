package nth.lib.integration.model.baoviet;

import lombok.Data;

@Data
public class BACert {
    private String fileName;
    private String type;
    private String content;
    private String contentStr;
}
