package nth.lib.integration.model.baoviet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BAFeeModel {
    private String checkReuse;

    @JsonProperty("checkTtskNdbh")
    private String checkInsuredHeathInfo;

    @JsonProperty("chuongTrinhBh")
    private String insuranceProgram;

    private String giftCodeAgency;

    private String giftCodeAgencyDiscount;

    private String insuranceTarget;

    @JsonProperty("ngoaitruChk")
    private String outpatientOption;

    @JsonProperty("ngoaitruPhi")
    private String outpatientFee;

    @JsonProperty("nguoidbhNgaysinh")
    private String insuredDob;

    @JsonProperty("nhakhoaChk")
    private String dentalOption;

    @JsonProperty("nhakhoaPhi")
    private String dentalFee;

    @JsonProperty("smcnChk")
    private String lifeOption;

    @JsonProperty("smcnPhi")
    private String lifeFee;

    @JsonProperty("smcnSotienbh")
    private String lifeValue;

    @JsonProperty("thaisanChk")
    private String maternityOption;

    @JsonProperty("thaisanPhi")
    private String maternityFee;

    @JsonProperty("tncnChk")
    private String accidentOption;

    @JsonProperty("tncnPhi")
    private String accidentFee;

    @JsonProperty("tncnSotienbh")
    private String accidentValue;

    private String oldPolicyNumber;

    private String promoCode;

    @JsonProperty("promoDes")
    private String promoDesc;

    private String promoPercent;

    private String promoValue;

    @JsonProperty("qlChinhPhi")
    private String mainFee;

    private String serial;

    @JsonProperty("thoihanbhTu")
    private String startDate;

    @JsonProperty("tongPhiBH")
    private String totalFee;

    @JsonProperty("tongPhiSauTangGiam")
    private String totalFeeAfterAdjust;

    @JsonProperty("tuoi")
    private String age;
}
