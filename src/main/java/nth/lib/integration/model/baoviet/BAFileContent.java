package nth.lib.integration.model.baoviet;

import lombok.Data;

@Data
public class BAFileContent {
    private String attachmentId;
    private String content;
    private String fileType;
    private String filename;
}
