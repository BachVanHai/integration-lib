package nth.lib.integration.model.baoviet;

import lombok.Data;

@Data
public class BAInvoiceInfo {
    private String accountNo;
    private String address;
    private String check;
    private String company;
    private String name;
    private String taxNo;
}
