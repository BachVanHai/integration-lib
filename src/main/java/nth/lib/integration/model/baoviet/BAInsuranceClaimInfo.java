package nth.lib.integration.model.baoviet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import nth.lib.integration.utils.BaoVietConstant;

import java.io.Serializable;
import java.util.List;

@Data
public class BAInsuranceClaimInfo implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 2L;

    private ReceiverUserInfo receiverUser;

    private BAInvoiceInfo invoiceInfo;

    private String agreementId;

    private String contactCode;

    private String contactName;

    private String contactDob;

    private String contactCategoryType;

    private String contactAddress;

    private String contactPhone;

    private String contactEmail;

    private String contactIdNumber;

    private String contactCif;

    private String receiveMethod;

    @JsonProperty("gycbhNumber")
    private String certNumber;

    private String lineId;

    private String statusPolicy;

    private String departmentId;

    private String checkOTP;

    @JsonProperty("kenhPhanPhoi")
    private String distributionChannel;

    private String confirmMethod;

    private String insuranceTarget;

    private String promoValue;

    @JsonProperty("thoihanbhTu")
    private String startDate;

    @JsonProperty("thoihanbhDen")
    private String endDate;

    @JsonProperty("nguoiycAddress")
    private String applicantAddress;

    @JsonProperty("nguoiycCmnd")
    private String applicantIcNumber;

    @JsonProperty("nguoiycEmail")
    private String applicantEmail;

    @JsonProperty("nguoiycName")
    private String applicantName;

    @JsonProperty("nguoiycNgaysinh")
    private String applicantDob;

    @JsonProperty("nguoiycPhone")
    private String applicantPhoneNumber;

    @JsonProperty("listBvgAddBaseVM")
    private List<BAInsured> insuredInfo;

    @JsonProperty("listBvgAddBaseVMAll")
    private List<BAInsured> insuredAllInfo;

    @JsonProperty("ttskCheck")
    private String checkHealthInfo;

    private String q1;

    private String q2;

    private String q3;

    private String q4;

    private String q1Question = BaoVietConstant.SURVEY_Q1;

    private String q2Question = BaoVietConstant.SURVEY_Q2;

    private String q3Question = BaoVietConstant.SURVEY_Q3;

    private String q4Question = BaoVietConstant.SURVEY_Q4;

    private String q1QuestionNote;

    private String q2QuestionNote;

    private String q3QuestionNote;

    private String q4QuestionNote;

    @JsonProperty("hasNguoinhantien")
    private boolean hasPayee;

    @JsonProperty("hasNguoithuhuong")
    private boolean hasBeneficiary;

    @JsonProperty("hasGks")
    private boolean hasBirthCert;

    @JsonProperty("hasTThoadonGTG")
    private boolean hasVATInfo;

    @JsonProperty("isShowDgrr")
    private boolean isShowRiskAssessment;

    private String totalPremium;

    private String netPremium;

    private String changePremium;

    @JsonProperty("totalPremiumDGRR")
    private String totalPremiumAfterRiskAssessment;

    private String guaranteeCard;

    @JsonProperty("soNguoiThamGia")
    private String numberOfParticipant;

    @JsonProperty("nguoinhanCmnd")
    private String payeeIcNumber;

    @JsonProperty("nguoinhanName")
    private String payeeName;

    @JsonProperty("nguoinhanQuanhe")
    private String relateOfInsuredAndPayee;

    @JsonProperty("nguointDiachi")
    private String payeeAddress;

    @JsonProperty("nguointDienthoai")
    private String payeePhoneNumber;

    @JsonProperty("nguointEmail")
    private String payeeEmail;

    @JsonProperty("nguointNgaysinh")
    private String payeeDob;

    private String agencyConfirmContent;

    private String dueDateInput;

    private String discount;

    private String endDateOld;

    @JsonProperty("fullnameDisplay")
    private String fullNameDisplay;

    @JsonProperty("gycFiles")
    private List<BAFileContent> insuranceClaimFiles;

    @JsonProperty("gycbhId")
    private String insuranceClaimId;

    @JsonProperty("maDaiLy")
    private String agencyCode;

    private String note;

    @JsonProperty("oldGycbhNumber")
    private String oldCertNumber;

    private String oldPolicyNumber;

    private String riskAssessmentContent;

    private String riskAssessmentDescription;

    private String riskAssessmentStatus;

    @JsonProperty("showGiamDinh")
    private String showAssessment;

    @JsonProperty("soHdBaoDam")
    private String guaranteeContractNumber;

    private String urlPolicy;

    @JsonProperty("urnCanBo")
    private String urlStaff;

    @JsonProperty("urnThamChieu")
    private String urlRef;

    @JsonProperty("agencyTextfield5")
    private String saleCode;

    private String policyNumber;

    private String language = "0";

//    @JsonProperty("nguoiThuHuong1")
//    private String beneficiaryOneName;

//    @JsonProperty("nguoiThuHuong2")
//    private String beneficiaryTwoName;

//    @JsonProperty("idNumberNguoiTh1")
//    private String beneficiaryOneIcNumber;

//    @JsonProperty("idNumberNguoiTh2")
//    private String beneficiaryTwoIcNumber;

//    @JsonProperty("maCanBoNganHang")
//    private String bankStaffCode;

//    @JsonProperty("tenCanBoNganHang")
//    private String bankStaffName;

//    @JsonProperty("emailCanBoNganHang")
//    private String bankStaffEmail;

//    @JsonProperty("maNhanVien")
//    private String refStaffCode;

//    @JsonProperty("tenNhanVienGT")
//    private String refStaffName;

//    @JsonProperty("emailNhanVienGT")
//    private String refStaffEmail;
}
