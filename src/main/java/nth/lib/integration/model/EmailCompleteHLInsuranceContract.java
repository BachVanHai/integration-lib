package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.MailTemplate;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
public class EmailCompleteHLInsuranceContract {
    private String certId;
    private String greeting;
    private String paymentStatus;
    private String status;
    private Set<String> beneficiaries = new HashSet<>();
    private String ownerName;
    private String insuranceType;
    private String insuranceCompany;
    private String insuranceStartDateTime;
    private String insuranceEndDateTime;
    private String totalFee;
    private String insuranceStatus;
    private String link;
    private String email;
    private String fileName;
    private MailTemplate emailTemplate;
    private Map<String, String> attachments;
}
