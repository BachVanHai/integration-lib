package nth.lib.integration.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonReqHeader {
    String deviceId;
    String sourceApp;
    String version;
    String language;
    String messageId;
    String userAgent;
    String remoteIp;
    String authorization;
    String locationId;
}
