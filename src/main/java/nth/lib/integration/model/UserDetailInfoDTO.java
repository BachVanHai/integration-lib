package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDetailInfoDTO implements Serializable {
    
    private Long id;

    private String bankAccount;

    private String bankName;

    private String bankBranch;

    private String address;

    private String ward;

    private String district;

    private String city;

    private String country;

    private String companyName;

    private String companyShortName;

    private String branchId;

    private String branchCode;

    private String branchName;

    private String staffCode;

    private String position;
}
