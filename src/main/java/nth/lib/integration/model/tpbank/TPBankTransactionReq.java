package nth.lib.integration.model.tpbank;

import lombok.Data;

@Data
public class TPBankTransactionReq {

    String username;
    String apiId;
    String checksum;
    String transactionKey;
    RequestBody requestBody;

    @Data
    public static class RequestBody {
        String partnerCode;
        String deviceCode;
        String userToken;
        String bookingId;
        String amount;
        String currency;
        String viewInfo;
        String description;
        String payload;
        String checksum;
        String customerXSale;
        String transactionType = "Buy";
        String periodicBilling = "NO";

        public String getCheckSumText(String secretKey) {
            return partnerCode + deviceCode + userToken + customerXSale + amount + description + viewInfo + payload + secretKey;
        }
    }
}
