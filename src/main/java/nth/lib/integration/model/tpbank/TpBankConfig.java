package nth.lib.integration.model.tpbank;


import lombok.Data;

@Data
public class TpBankConfig {
    String secretId;
    String clientId;
    String rootUrl;
    String authUrl;
    String xsaleUrl;
}
