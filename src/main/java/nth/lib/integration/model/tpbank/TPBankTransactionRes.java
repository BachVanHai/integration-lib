package nth.lib.integration.model.tpbank;

import lombok.Data;

import java.util.HashMap;

@Data
public class TPBankTransactionRes {

    String transactionId;
    ErrorMessage globalErrorMessage;
    ResponseBody responseBody;

    @Data
    public static class ResponseBody {
        String id;
        String userToken;
        String checksum;
    }

    @Data
    public static class ErrorMessage {
        String errorCode;
        String errorDesc;
        HashMap<String, String> messages;
    }
}
