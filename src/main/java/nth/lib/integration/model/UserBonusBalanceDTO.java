package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UserBonusBalanceDTO implements Serializable {

    private Long id;
    private BigDecimal totalBonusValue;
    private String userId;
    private String userCode;
}
