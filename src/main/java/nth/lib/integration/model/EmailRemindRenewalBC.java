package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.enumeration.MailTemplate;

@Data
public class EmailRemindRenewalBC {
    private String greeting;
    private String insuranceCompany;
    private String expiredDate;
    private String insuranceType;
    private String totalFee;
    private String email;
    private String link;
    private String userId;
    private ContractType contractType;
    private String contractCode;
    private String insuranceStartDateTime;
    private String insuranceEndDateTime;
    private String status;
    private MailTemplate mailTemplate=MailTemplate.INON_EXPIRED_CONTRACT_BC;

}
