package nth.lib.integration.model.ExcelToModel.vehicle;

import lombok.Data;
import nth.lib.integration.enumeration.ICType;
import nth.lib.integration.enumeration.Usage;
import nth.lib.integration.enumeration.VehicleStatus;
import nth.lib.integration.enumeration.VehicleType;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class VehicleCarExcel {

    private String numberPlate;
    private VehicleStatus vehicleStatus;
    private String vehicleType;
    private String manufactureName;
    private String branchName;
    private Usage usage;
    private String frameNo;
    private String machineNo;
    private BigDecimal initValue;
    private String issPlace;
    private String issDate;
    private String duration;
    private Instant startValueDate;
    private BigDecimal people;
    private BigDecimal asset;
    private BigDecimal passenger;
    private BigDecimal mTnTgT;
    private int mountOfPeople;
    private int load;
    private String vehicleCode;
    private String fullName;
    private ICType icType;
    private String icNo;
    private String telephone;
    private String email;
    private String address;
}
