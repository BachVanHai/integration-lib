package nth.lib.integration.model.ExcelToModel.home;

import lombok.Data;
import nth.lib.integration.enumeration.*;

@Data
public class HomeInsuranceExcel {

    private HouseOwnerType houseOwnerType;
    private ICType icType;
    private String icNo;
    private String issDate;
    private String issPlace;
    private String fullName;
    private String telephone;
    private String dateOfBirth;
    private String address;
    private String email;
    private Gender gender;
    private HouseType houseType;
    private int usedTime;
    private String houseAddress;
    private int duration;
    private String startValueDate;
    private String assetLiability;
    private String materialLiability;
    private Coverage coverage;
    private String bankName;
    private String bankAddress;
    private String bankBranch;
    private String transferLevel;

}
