package nth.lib.integration.model.ExcelToModel.personal;

import lombok.Data;
import nth.lib.integration.enumeration.Gender;
import nth.lib.integration.enumeration.ICType;
import nth.lib.integration.enumeration.RelationShip;

@Data
public class VTAExcel {

    private String fullName;
    private ICType icType;
    private String icNo;
    private String dateOfBirth;
    private Gender gender;
    private String telephone;
    private String email;
    private String address;
    private RelationShip relationShip;
    private String packageInsuranceName;
    private int duration;
    private String startValueDate;
    private Boolean isBuyer;
}
