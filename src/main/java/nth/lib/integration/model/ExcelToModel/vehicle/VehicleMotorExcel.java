package nth.lib.integration.model.ExcelToModel.vehicle;

import lombok.Data;
import nth.lib.integration.enumeration.ICType;
import nth.lib.integration.enumeration.VehicleType;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class VehicleMotorExcel {

    private String numberPlate;
    private VehicleType vehicleType;
    private String frameNo;
    private String machineNo;
    private String manufactureName;
    private String branchName;
    private String fullName;
    private ICType icType;
    private String icNo;
    private String telephone;
    private String email;
    private String address;
    private int duration;
    private Instant startValueDate;
    private BigDecimal mTnTgT;
    private int mountOfPeople;
    private String vehicleCode;
}
