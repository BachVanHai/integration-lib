package nth.lib.integration.model.ExcelToModel.home;

import nth.lib.integration.enumeration.PaymentType;
import nth.lib.integration.enumeration.Unit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class HomeInsuranceDTO implements Serializable {

    private Long id;

    private String productCode;

    private String packageCode;

    private String insuranceCode;

    private Long duration;

    private Instant startedDate;

    private Instant endDate;

    private Boolean materialOption;

    private Boolean assetOption;

    private BigDecimal deduct;

    private Long beneficiaryId;

    private String beneficiaryName;

    private PaymentType paymentType;

    private String promoCode;

    private BigDecimal addedFee;

    private BigDecimal reduceFee;

    private BigDecimal amount;

    private Unit unit;

    private Boolean isEnable;

    private Instant createdDate;

    private String createdBy;

    private Instant updateDate;

    private String updateBy;

    private Long insuranceTypeInsuranceId;

    private HomeInsuranceTypeDTO insuranceTypeInsurance;

    private String contractInsurancesId;

    private HomeInsuranceAddOnDTO insuranceAddOnMaterial;

    private HomeInsuranceAddOnDTO insuranceAddOnAsset;

    private Boolean bankOption;

    private HomeBeneficiaryBankDTO beneficiaryBankDTO;

    private BigDecimal feeMaterial;

    private BigDecimal feeAsset;

    public BigDecimal getFeeMaterial() {
        return feeMaterial;
    }

    public void setFeeMaterial(BigDecimal feeMaterial) {
        this.feeMaterial = feeMaterial;
    }

    public BigDecimal getFeeAsset() {
        return feeAsset;
    }

    public void setFeeAsset(BigDecimal feeAsset) {
        this.feeAsset = feeAsset;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(String insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public Boolean getBankOption() {
        return bankOption;
    }

    public void setBankOption(Boolean bankOption) {
        this.bankOption = bankOption;
    }

    public HomeBeneficiaryBankDTO getBeneficiaryBankDTO() {
        return this.beneficiaryBankDTO;
    }

    public void setBeneficiaryBankDTO(HomeBeneficiaryBankDTO beneficiaryBankDTO) {
        this.beneficiaryBankDTO = beneficiaryBankDTO;
    }

    public HomeInsuranceAddOnDTO getInsuranceAddOnMaterial() {
        return insuranceAddOnMaterial;
    }

    public void setInsuranceAddOnMaterial(HomeInsuranceAddOnDTO insuranceAddOnMaterial) {
        this.insuranceAddOnMaterial = insuranceAddOnMaterial;
    }

    public HomeInsuranceAddOnDTO getInsuranceAddOnAsset() {
        return insuranceAddOnAsset;
    }

    public void setInsuranceAddOnAsset(HomeInsuranceAddOnDTO insuranceAddOnAsset) {
        this.insuranceAddOnAsset = insuranceAddOnAsset;
    }

    public Long getInsuranceTypeInsuranceId() {
        return insuranceTypeInsuranceId;
    }

    public void setInsuranceTypeInsuranceId(Long insuranceTypeInsuranceId) {
        this.insuranceTypeInsuranceId = insuranceTypeInsuranceId;
    }

    public String getContractInsurancesId() {
        return contractInsurancesId;
    }

    public void setContractInsurancesId(String contractInsurancesId) {
        this.contractInsurancesId = contractInsurancesId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Instant getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Instant startedDate) {
        this.startedDate = startedDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Boolean getMaterialOption() {
        return materialOption;
    }

    public void setMaterialOption(Boolean materialOption) {
        this.materialOption = materialOption;
    }

    public Boolean getAssetOption() {
        return assetOption;
    }

    public void setAssetOption(Boolean assetOption) {
        this.assetOption = assetOption;
    }

    public BigDecimal getDeduct() {
        return deduct;
    }

    public void setDeduct(BigDecimal deduct) {
        this.deduct = deduct;
    }

    public Long getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Long beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public BigDecimal getAddedFee() {
        return addedFee;
    }

    public void setAddedFee(BigDecimal addedFee) {
        this.addedFee = addedFee;
    }

    public BigDecimal getReduceFee() {
        return reduceFee;
    }

    public void setReduceFee(BigDecimal reduceFee) {
        this.reduceFee = reduceFee;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public HomeInsuranceTypeDTO getInsuranceTypeInsurance() {
        return insuranceTypeInsurance;
    }

    public void setInsuranceTypeInsurance(HomeInsuranceTypeDTO insuranceTypeInsurance) {
        this.insuranceTypeInsurance = insuranceTypeInsurance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsuranceDTO{" +
            "id=" + getId() +
            ", duration=" + getDuration() +
            ", startedDate='" + getStartedDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", materialOption='" + getMaterialOption() + "'" +
            ", assetOption='" + getAssetOption() + "'" +
            ", deduct=" + getDeduct() +
            ", beneficiaryId=" + getBeneficiaryId() +
            ", beneficiaryName='" + getBeneficiaryName() + "'" +
            ", paymentChannel='" + getPaymentType() + "'" +
            ", promoCode='" + getPromoCode() + "'" +
            ", addedFee=" + getAddedFee() +
            ", reduceFee=" + getReduceFee() +
            ", amount=" + getAmount() +
            ", unit='" + getUnit() + "'" +
            ", isEnable='" + getIsEnable() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", insuranceTypeInsurance=" + getInsuranceTypeInsurance() +
            "}";
    }
}
