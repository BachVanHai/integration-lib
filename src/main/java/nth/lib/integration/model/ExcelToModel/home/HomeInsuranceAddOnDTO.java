package nth.lib.integration.model.ExcelToModel.home;

import nth.lib.integration.enumeration.Coverage;
import nth.lib.integration.enumeration.HomeInsuranceAddOnType;
import nth.lib.integration.enumeration.Unit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class HomeInsuranceAddOnDTO implements Serializable {

    private Long id;

    private BigDecimal compensationLimit;

    private Coverage coverage;

    private HomeInsuranceAddOnType insuranceAddOn;

    private Unit unit;

    private Long insuranceInsuranceAddOnsId;

    private HomeInsuranceDTO insuranceInsuranceAddOns;

    public HomeInsuranceAddOnType getInsuranceAddOn() {
        return insuranceAddOn;
    }

    public void setInsuranceAddOn(HomeInsuranceAddOnType insuranceAddOn) {
        this.insuranceAddOn = insuranceAddOn;
    }

    public HomeInsuranceDTO getInsuranceInsuranceAddOns() {
        return insuranceInsuranceAddOns;
    }

    public void setInsuranceInsuranceAddOns(HomeInsuranceDTO insuranceInsuranceAddOns) {
        this.insuranceInsuranceAddOns = insuranceInsuranceAddOns;
    }

    public Long getInsuranceInsuranceAddOnsId() {
        return insuranceInsuranceAddOnsId;
    }

    public void setInsuranceInsuranceAddOnsId(Long insuranceInsuranceAddOnsId) {
        this.insuranceInsuranceAddOnsId = insuranceInsuranceAddOnsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCompensationLimit() {
        return compensationLimit;
    }

    public void setCompensationLimit(BigDecimal compensationLimit) {
        this.compensationLimit = compensationLimit;
    }

    public Coverage getCoverage() {
        return coverage;
    }

    public void setCoverage(Coverage coverage) {
        this.coverage = coverage;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HomeInsuranceAddOnDTO)) {
            return false;
        }

        HomeInsuranceAddOnDTO homeInsuranceAddOnDTO = (HomeInsuranceAddOnDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, homeInsuranceAddOnDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsuranceAddOnDTO{" +
            "id=" + getId() +
            ", compensationLimit=" + getCompensationLimit() +
            ", coverage='" + getCoverage() + "'" +
            ", insuranceAddOn='" + getInsuranceAddOn() + "'" +
            ", unit='" + getUnit() + "'" +
            ", insuranceInsuranceAddOns=" + getInsuranceInsuranceAddOns() +
            "}";
    }
}
