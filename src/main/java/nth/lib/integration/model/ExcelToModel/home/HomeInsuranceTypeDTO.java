package nth.lib.integration.model.ExcelToModel.home;


import nth.lib.integration.enumeration.Unit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


public class HomeInsuranceTypeDTO implements Serializable {

    private Long id;

    private String code;

    private Boolean isEnabled;

    private String description;

    private String appId;

    private String companyId;

    private BigDecimal rateMaterial;

    private BigDecimal rateAsset;

    private Unit type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public BigDecimal getRateMaterial() {
        return rateMaterial;
    }

    public void setRateMaterial(BigDecimal rateMaterial) {
        this.rateMaterial = rateMaterial;
    }

    public BigDecimal getRateAsset() {
        return rateAsset;
    }

    public void setRateAsset(BigDecimal rateAsset) {
        this.rateAsset = rateAsset;
    }

    public Unit getType() {
        return type;
    }

    public void setType(Unit type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HomeInsuranceTypeDTO)) {
            return false;
        }

        HomeInsuranceTypeDTO homeInsuranceTypeDTO = (HomeInsuranceTypeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, homeInsuranceTypeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsuranceTypeDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", isEnabled='" + getIsEnabled() + "'" +
            ", description='" + getDescription() + "'" +
            ", appId='" + getAppId() + "'" +
            ", companyId='" + getCompanyId() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
