package nth.lib.integration.model.ExcelToModel.vehicle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nth.lib.integration.enumeration.ICType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCarExcel {

    private String numberPlate;
    private String fullName;
    private ICType icType;
    private String icNo;
    private String telephone;
    private String email;
    private String address;
}
