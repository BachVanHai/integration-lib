package nth.lib.integration.model.ExcelToModel.home;

import java.io.Serializable;
import java.util.Objects;


public class HouseAddressDTO implements Serializable {

    private Long id;

    private String city;

    private String district;

    private String ward;

    private String detail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HouseAddressDTO)) {
            return false;
        }

        HouseAddressDTO houseAddressDTO = (HouseAddressDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, houseAddressDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HouseAddressDTO{" +
            "id=" + getId() +
            ", city='" + getCity() + "'" +
            ", district='" + getDistrict() + "'" +
            ", ward='" + getWard() + "'" +
            ", detail='" + getDetail() + "'" +
            "}";
    }
}
