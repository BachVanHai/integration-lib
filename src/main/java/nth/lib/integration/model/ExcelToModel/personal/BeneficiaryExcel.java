package nth.lib.integration.model.ExcelToModel.personal;

import lombok.Data;
import nth.lib.integration.enumeration.CustomerType;
import nth.lib.integration.enumeration.Gender;
import nth.lib.integration.enumeration.ICType;

import java.time.Instant;
import java.time.LocalDate;

@Data
public class BeneficiaryExcel {

    private Long id;

    private ICType icType;

    private String icNo;

    private LocalDate issuedDate;

    private String issuedPlace;

    private String fullName;

    private String branchName;

    private LocalDate dateOfBirth;

    private Gender gender;

    private String phoneNumber;

    private String email;

    private String address;

    private CustomerType type;

    private String createdBy;

    private Instant createdAt;

    private String updatedBy;

    private Instant updatedAt;

    private Boolean isBuyer;

    private String contractId;

    private String packageName;

    private Integer duration;

    private String ref_id;

    private Long idInsurance;

    private String bankName;

    private String accountNumber;

    private String startValueDate;
}
