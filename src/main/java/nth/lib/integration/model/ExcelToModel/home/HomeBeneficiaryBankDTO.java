package nth.lib.integration.model.ExcelToModel.home;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class HomeBeneficiaryBankDTO implements Serializable {

    private Long id;

    private String name;

    private String branch;

    private String address;

    private BigDecimal benefitTransferRate;

    private Long insuranceBeneficiaryBankId;

    public Long getInsuranceBeneficiaryBankId() {
        return insuranceBeneficiaryBankId;
    }

    public void setInsuranceBeneficiaryBankId(Long insuranceBeneficiaryBankId) {
        this.insuranceBeneficiaryBankId = insuranceBeneficiaryBankId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getBenefitTransferRate() {
        return benefitTransferRate;
    }

    public void setBenefitTransferRate(BigDecimal benefitTransferRate) {
        this.benefitTransferRate = benefitTransferRate;
    }


    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BeneficiaryBankDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", branch='" + getBranch() + "'" +
            ", address='" + getAddress() + "'" +
            ", benefitTransferRate=" + getBenefitTransferRate() +
            "}";
    }
}
