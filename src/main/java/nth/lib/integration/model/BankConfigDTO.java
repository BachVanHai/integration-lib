package nth.lib.integration.model;

import java.io.Serializable;

/**
 * A DTO for the {@link nth.module.insurancecompany.domain.BankConfig} entity.
 */
public class BankConfigDTO implements Serializable {

    private Long id;

    private String appName;

    private String bankName;

    private String accountNo;

    private String branch;

    private String accountOwner;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankConfigDTO)) {
            return false;
        }

        return id != null && id.equals(((BankConfigDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankConfigDTO{" +
            "id=" + getId() +
            ", appName='" + getAppName() + "'" +
            ", bankName='" + getBankName() + "'" +
            ", accountNo='" + getAccountNo() + "'" +
            ", branch='" + getBranch() + "'" +
            ", accountOwner='" + getAccountOwner() + "'" +
            "}";
    }
}
