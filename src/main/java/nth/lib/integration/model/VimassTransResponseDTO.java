package nth.lib.integration.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

public class VimassTransResponseDTO implements Serializable {

    private Long id;

    private String contractCode;

    private BigDecimal amount;

    private String error;

    private Instant responseTime;

    private String status;

    private String idVimass;

    public String getIdVimass() {
        return idVimass;
    }

    public void setIdVimass(String idVimass) {
        this.idVimass = idVimass;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Instant getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Instant responseTime) {
        this.responseTime = responseTime;
    }
}
