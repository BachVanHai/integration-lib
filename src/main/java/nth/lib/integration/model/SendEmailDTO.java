package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.MailTemplate;

import java.util.Map;


@Data
public class SendEmailDTO {
    MailTemplate template;
    String subject;
    Object mailVariables;
    String receiverMail;
    Map<String, String> fileData;
}
