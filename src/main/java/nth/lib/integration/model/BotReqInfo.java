package nth.lib.integration.model;

import lombok.Data;

@Data
public class BotReqInfo {
    private String command;
    private String contractCode;
    private String value;
}
