package nth.lib.integration.model;

import lombok.Data;

import java.util.List;

@Data
public class DebtDataNotification {
    String email;
    String fullName;
    List<DebtContractInfo> contracts;
    String total;
}
