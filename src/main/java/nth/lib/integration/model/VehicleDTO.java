package nth.lib.integration.model;

import nth.lib.integration.enumeration.PlaceType;
import nth.lib.integration.enumeration.Usage;
import nth.lib.integration.enumeration.VehicleStatus;
import nth.lib.integration.enumeration.VehicleType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

public class VehicleDTO implements Serializable {

    private Long id;

    private VehicleStatus vehicleStatus;

    private String numberPlate;

    private Usage usage;

    private PlaceType issPlace;

    private LocalDate issDate;


    private String manufacturerName;

    private String brandName;

    private BigDecimal initValue;

    private String frameNo;

    private String machineNo;

    private Instant createdDate;

    private String createdBy;

    private Boolean isEnable;

//    @VehicleRequired(vehicleType = {VehicleType.CAR})
    private Integer seats;

//    @VehicleRequired(vehicleType = {VehicleType.CAR})
    private BigDecimal loads;

    private BigDecimal contractValue;


    private String contractId;

    private Long vehicleTypeId;

    private String vehicleNote;

    private VehicleTypeDTO vehicleTypeDTO;

    public VehicleTypeDTO getVehicleTypeDTO() {
        return vehicleTypeDTO;
    }

    public void setVehicleTypeDTO(VehicleTypeDTO vehicleTypeDTO) {
        this.vehicleTypeDTO = vehicleTypeDTO;
    }

    public String getVehicleNote() {
        return vehicleNote;
    }

    public void setVehicleNote(String vehicleNote) {
        this.vehicleNote = vehicleNote;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleStatus getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(VehicleStatus vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public PlaceType getIssPlace() {
        return issPlace;
    }

    public void setIssPlace(PlaceType issPlace) {
        this.issPlace = issPlace;
    }

    public LocalDate getIssDate() {
        return issDate;
    }

    public void setIssDate(LocalDate issDate) {
        this.issDate = issDate;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public BigDecimal getInitValue() {
        return initValue;
    }

    public void setInitValue(BigDecimal initValue) {
        this.initValue = initValue;
    }

    public String getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(String frameNo) {
        this.frameNo = frameNo;
    }

    public String getMachineNo() {
        return machineNo;
    }

    public void setMachineNo(String machineNo) {
        this.machineNo = machineNo;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public BigDecimal getLoads() {
        return loads;
    }

    public void setLoads(BigDecimal loads) {
        this.loads = loads;
    }

    public BigDecimal getContractValue() {
        return contractValue;
    }

    public void setContractValue(BigDecimal contractValue) {
        this.contractValue = contractValue;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VehicleDTO)) {
            return false;
        }

        return id != null && id.equals(((VehicleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleDTO{" +
            "id=" + getId() +
            ", vehicleStatus='" + getVehicleStatus() + "'" +
            ", numberPlate='" + getNumberPlate() + "'" +
            ", usage='" + getUsage() + "'" +
            ", issPlace='" + getIssPlace() + "'" +
            ", issDate='" + getIssDate() + "'" +
            ", manufacturerName='" + getManufacturerName() + "'" +
            ", brandName='" + getBrandName() + "'" +
            ", initValue=" + getInitValue() +
            ", frameNo='" + getFrameNo() + "'" +
            ", machineNo='" + getMachineNo() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", seats=" + getSeats() +
            ", loads=" + getLoads() +
            ", contractValue=" + getContractValue() +
            ", contractId=" + getContractId() +
            ", vehicleTypeId=" + getVehicleTypeId() +
            "}";
    }
}
