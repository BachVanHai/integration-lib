package nth.lib.integration.model;


import java.time.Instant;

public class FileUploadResponse {
    private Long id;
    private String code;
    private String contractId;
    private String name;
    private String path;
    private String type;
    private Instant uploadDate;
    private String uploadBy;
    private String userId;
    private String deviceId;
    private String ipServer;
    private String ipClientUpload;
    private String checksum;
    private String docType;
    private Integer version;
    private Instant expiredDate;
    private String downloadFromUrl;
    private String fileValidation;
    private Boolean isEnabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Instant getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Instant uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    public String getIpClientUpload() {
        return ipClientUpload;
    }

    public void setIpClientUpload(String ipClientUpload) {
        this.ipClientUpload = ipClientUpload;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Instant getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Instant expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getDownloadFromUrl() {
        return downloadFromUrl;
    }

    public void setDownloadFromUrl(String downloadFromUrl) {
        this.downloadFromUrl = downloadFromUrl;
    }

    public String getFileValidation() {
        return fileValidation;
    }

    public void setFileValidation(String fileValidation) {
        this.fileValidation = fileValidation;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }
}
