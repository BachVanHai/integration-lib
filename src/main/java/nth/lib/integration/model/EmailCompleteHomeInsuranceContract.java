package nth.lib.integration.model;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class EmailCompleteHomeInsuranceContract {
    private String certId;
    private String greeting;
    private String paymentStatus;
    private String status;
    private Set<String> beneficiaries = new HashSet<>();
    private String ownerName;
    private String insuranceType;
    private String insuranceCompany;
    private String insuranceDuration;
    private String insuranceStartDateTime;
    private String insuranceEndDateTime;
    private String totalFee;
    private String insuranceStatus;
    private String link;
    private String email;
    private String fileName;
}
