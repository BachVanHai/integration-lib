package nth.lib.integration.model;

import nth.lib.integration.enumeration.CertStatus;
import nth.lib.integration.enumeration.CertType;
import nth.lib.integration.enumeration.PolicyType;

import java.io.Serializable;
import java.time.Instant;

public class CertificateDTO implements Serializable {

    private Long id;

    private String insurCompanyId;

    private String url;

    private String typeCode;

    private String productCode;

    private String contractNo;

    private String certSerialPart1;

    private String certSerialPart2;

    private String certSerialPart3;

    private String contractId;

    private CertType certType;

    private PolicyType policyType;

    private String failCode;

    private Long workFlowId;

    private Long approvalId;

    private String latestStatus;

    private CertStatus status;

    private Boolean isDeleted;

    private Instant createdDate;

    private String createdBy;

    private Instant updateDate;

    private String updatedBy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInsurCompanyId() {
        return insurCompanyId;
    }

    public void setInsurCompanyId(String insurCompanyId) {
        this.insurCompanyId = insurCompanyId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCertSerialPart1() {
        return certSerialPart1;
    }

    public void setCertSerialPart1(String certSerialPart1) {
        this.certSerialPart1 = certSerialPart1;
    }

    public String getCertSerialPart2() {
        return certSerialPart2;
    }

    public void setCertSerialPart2(String certSerialPart2) {
        this.certSerialPart2 = certSerialPart2;
    }

    public String getCertSerialPart3() {
        return certSerialPart3;
    }

    public void setCertSerialPart3(String certSerialPart3) {
        this.certSerialPart3 = certSerialPart3;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public CertType getCertType() {
        return certType;
    }

    public void setCertType(CertType certType) {
        this.certType = certType;
    }

    public PolicyType getPolicyType() {
        return policyType;
    }

    public void setPolicyType(PolicyType policyType) {
        this.policyType = policyType;
    }

    public String getFailCode() {
        return failCode;
    }

    public void setFailCode(String failCode) {
        this.failCode = failCode;
    }

    public Long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(Long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }

    public CertStatus getStatus() {
        return status;
    }

    public void setStatus(CertStatus status) {
        this.status = status;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CertificateDTO)) {
            return false;
        }

        return id != null && id.equals(((CertificateDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CertificateDTO{" +
            "id=" + getId() +
            ", insurCompanyId='" + getInsurCompanyId() + "'" +
            ", url='" + getUrl() + "'" +
            ", typeCode='" + getTypeCode() + "'" +
            ", productCode='" + getProductCode() + "'" +
            ", contractNo='" + getContractNo() + "'" +
            ", certSerialPart1='" + getCertSerialPart1() + "'" +
            ", certSerialPart2='" + getCertSerialPart2() + "'" +
            ", certSerialPart3='" + getCertSerialPart3() + "'" +
            ", contractId='" + getContractId() + "'" +
            ", certType='" + getCertType() + "'" +
            ", policyType='" + getPolicyType() + "'" +
            ", failCode='" + getFailCode() + "'" +
            ", workFlowId=" + getWorkFlowId() +
            ", approvalId=" + getApprovalId() +
            ", latestStatus='" + getLatestStatus() + "'" +
            ", status='" + getStatus() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
