package nth.lib.integration.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LangMessage {
    String en;
    String vi;
}
