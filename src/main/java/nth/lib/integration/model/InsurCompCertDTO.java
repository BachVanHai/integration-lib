package nth.lib.integration.model;

import java.io.Serializable;
import java.time.Instant;

public class InsurCompCertDTO implements Serializable {

    private Long id;

    private String urlInsurComp;

    private String urlInOn;

    private String insurCompContractNo;

    private String insurCompContractId;

    private String printedCertNo;

    private String electCertNo;

    private String errorCode;

    private String errorDesc;

    private Instant responseTime;

    private String contractId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrlInsurComp() {
        return urlInsurComp;
    }

    public void setUrlInsurComp(String urlInsurComp) {
        this.urlInsurComp = urlInsurComp;
    }

    public String getUrlInOn() {
        return urlInOn;
    }

    public void setUrlInOn(String urlInOn) {
        this.urlInOn = urlInOn;
    }

    public String getInsurCompContractNo() {
        return insurCompContractNo;
    }

    public void setInsurCompContractNo(String insurCompContractNo) {
        this.insurCompContractNo = insurCompContractNo;
    }

    public String getInsurCompContractId() {
        return insurCompContractId;
    }

    public void setInsurCompContractId(String insurCompContractId) {
        this.insurCompContractId = insurCompContractId;
    }

    public String getPrintedCertNo() {
        return printedCertNo;
    }

    public void setPrintedCertNo(String printedCertNo) {
        this.printedCertNo = printedCertNo;
    }

    public String getElectCertNo() {
        return electCertNo;
    }

    public void setElectCertNo(String electCertNo) {
        this.electCertNo = electCertNo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public Instant getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Instant responseTime) {
        this.responseTime = responseTime;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InsurCompCertDTO)) {
            return false;
        }

        return id != null && id.equals(((InsurCompCertDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsurCompCertDTO{" +
            "id=" + getId() +
            ", urlInsurComp='" + getUrlInsurComp() + "'" +
            ", urlInOn='" + getUrlInOn() + "'" +
            ", insurCompContractNo='" + getInsurCompContractNo() + "'" +
            ", insurCompContractId='" + getInsurCompContractId() + "'" +
            ", printedCertNo='" + getPrintedCertNo() + "'" +
            ", electCertNo='" + getElectCertNo() + "'" +
            ", errorCode='" + getErrorCode() + "'" +
            ", errorDesc='" + getErrorDesc() + "'" +
            ", responseTime='" + getResponseTime() + "'" +
            "}";
    }
}
