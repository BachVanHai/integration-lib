
package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BSHApproveCert {

    @JsonProperty("ma_dvi")
    private String maDvi;
    @JsonProperty("so_id")
    private String soId;
    @JsonProperty("cb_du")
    private String cbDu;

}
