package nth.lib.integration.model;

/**
 * The AccountStatus enumeration.
 */
public class PayErrorCode {
    public static final String SUCCESS = "000";
    public static final String ERROR = "001";
    public static final String OVER_LIMIT= "002";
    public static final String TRANSACTION_EXISTED= "003";
}
