package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UserInsuranceSettingsDTO implements Serializable {
    
    private Long id;

    private BigDecimal value;

    private Boolean isEnabled;

    private Long userId;

    private Long insuranceId;
    

}
