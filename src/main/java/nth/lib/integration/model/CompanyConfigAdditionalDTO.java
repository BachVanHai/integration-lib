package nth.lib.integration.model;

import java.io.Serializable;

public class CompanyConfigAdditionalDTO implements Serializable {

    private Long id;

    private String key;

    private String value;


    private Long companyConfigId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getCompanyConfigId() {
        return companyConfigId;
    }

    public void setCompanyConfigId(Long companyConfigId) {
        this.companyConfigId = companyConfigId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyConfigAdditionalDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyConfigAdditionalDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyConfigAdditionalDTO{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", value='" + getValue() + "'" +
            ", companyConfigId=" + getCompanyConfigId() +
            "}";
    }
}
