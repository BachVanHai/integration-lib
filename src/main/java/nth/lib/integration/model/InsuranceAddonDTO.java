package nth.lib.integration.model;

import nth.lib.integration.enumeration.AddonType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

public class InsuranceAddonDTO implements Serializable {

    private Long id;

    private String addonCode;

    private Boolean isEnable;

    private String addonDesc;

    private BigDecimal value;

    private AddonType type;

    private String unit;

    private UUID contractId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddonCode() {
        return addonCode;
    }

    public void setAddonCode(String addonCode) {
        this.addonCode = addonCode;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public String getAddonDesc() {
        return addonDesc;
    }

    public void setAddonDesc(String addonDesc) {
        this.addonDesc = addonDesc;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public AddonType getType() {
        return type;
    }

    public void setType(AddonType type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public UUID getContractId() {
        return contractId;
    }

    public void setContractId(UUID contractId) {
        this.contractId = contractId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InsuranceAddonDTO)) {
            return false;
        }

        return id != null && id.equals(((InsuranceAddonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsuranceAddonDTO{" +
            "id=" + getId() +
            ", addonCode='" + getAddonCode() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", addonDesc='" + getAddonDesc() + "'" +
            ", value=" + getValue() +
            ", type='" + getType() + "'" +
            ", unit='" + getUnit() + "'" +
            ", contractId=" + getContractId() +
            "}";
    }
}
