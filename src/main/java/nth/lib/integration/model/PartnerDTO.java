package nth.lib.integration.model;

import java.io.Serializable;
import java.time.Instant;

public class PartnerDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private String secretKey;

    private String clientId;

    private Boolean isEnable;

    private Boolean isDeleted;

    private Long rateLimitBySeconds;

    private Long rateLimitByHours;

    private Long rateLimitByDays;

    private Instant createdDate;

    private String createdBy;

    private Instant updatedDate;

    private String updatedBy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getRateLimitBySeconds() {
        return rateLimitBySeconds;
    }

    public void setRateLimitBySeconds(Long rateLimitBySeconds) {
        this.rateLimitBySeconds = rateLimitBySeconds;
    }

    public Long getRateLimitByHours() {
        return rateLimitByHours;
    }

    public void setRateLimitByHours(Long rateLimitByHours) {
        this.rateLimitByHours = rateLimitByHours;
    }

    public Long getRateLimitByDays() {
        return rateLimitByDays;
    }

    public void setRateLimitByDays(Long rateLimitByDays) {
        this.rateLimitByDays = rateLimitByDays;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerDTO)) {
            return false;
        }

        return id != null && id.equals(((PartnerDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", secretKey='" + getSecretKey() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", rateLimitBySeconds=" + getRateLimitBySeconds() +
            ", rateLimitByHours=" + getRateLimitByHours() +
            ", rateLimitByDays=" + getRateLimitByDays() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
