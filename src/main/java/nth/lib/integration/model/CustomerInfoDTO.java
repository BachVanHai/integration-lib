package nth.lib.integration.model;


import lombok.Data;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

@Data
public class CustomerInfoDTO implements Serializable {

    private Long id;

    private String icType;

    private String icNo;

    private LocalDate issuedDate;

    private String issuedPlace;

    private String fullName;

    private String branchName;

    private LocalDate dateOfBirth;

    private String gender;

    private String phoneNumber;

    private String email;

    private String address;

    private String type;

    private String createdBy;

    private Instant createdAt;

    private String updatedBy;

    private Instant updatedAt;


}
