package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CheckVehicleInfoDTO implements Serializable {
    private String brand;
    private String name;
    private String city;
    private String codePlate;
}
