package nth.lib.integration.model;

import lombok.Data;

@Data
public class EmailCompleteCarContract {
    private String certId;
    private String greeting;
    private String paymentStatus;
    private String status;
    private String customerName;
    private String ownerName;
    private String carAutomakerName;
    private String carBrandName;
    private String carNumPlate;
    private String insuranceType;
    private String insuranceCompany;
    private String insuranceDuration;
    private String insuranceStartDateTime;
    private String insuranceEndDateTime;
    private String totalFee;
    private String insuranceStatus;
    private String link;
    private String link1;
    private String email;
    private String fileName;
}
