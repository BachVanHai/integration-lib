package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class BSHFileUploadResponse implements Serializable {
    @JsonProperty("Status")
    private String status;
    @JsonProperty("ErrCode")
    private String errorCode;
    @JsonProperty("Message")
    private String Message;
}
