package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.*;
import nth.lib.integration.utils.InsuranceFeeUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Data
public class ContractDTO implements Serializable {

    private String id;

    private String contractCode;

    private ContractType contractType;

    private BigDecimal contractValue;

    private BigDecimal vehicleValue;

    private String vehicleCode;

    private String salerId;

    private UsersDTO saler;

    private Long contractWorkFlowId;

    private Long contractApprovalId;

    private String latestApprovalStatus;

    private String companyId;

    private BigDecimal totalFeeInclVAT;

    private Unit feeUnit;

    private Boolean isSaveTemplate;

    private String templateName;

    private BigDecimal deduction;

    private Boolean isDeductible;

    private Long ownerId;

    private CustomerInfoDTO owner;

    private Boolean isChangeBeneficiary;

    private Long beneficiaryId;

    private BigDecimal benefitValue;

    private PaymentType paymentType;

    private Long paymentId;

    private String couponCode;

    private PrintedCertType printedCertType;

    private Long certReceiverId;

    private String certNo;

    private TaxInvoiceType taxInvoiceType;

    private Long taxInvoiceReceiverId;

    private Instant createdDate;

    private String createdBy;

    private Instant updateDate;

    private String updatedBy;

    private Long insurCompCertId;

    private String inonCertUrl;

    private String insurCompContractId;

    private Set<InsuranceDTO> insurances = new HashSet<>();

    private Set<InsuranceAddonDTO> insuranceAddons = new HashSet<>();

    private Set<VehicleDTO> vehicles = new HashSet<>();

    private String appId;

    private String refId;

    private String paid;

    private String contractGroupId;

    private String certUrl;

    private String salerName;

    private String salerCode;

    private String insurCompanyName;

    private InsurCompCertDTO insurCompCertDTO;

    private String tnpxnnOto;

    private String bhvcOto;

    private String tndstnOto;

    private String tndsbbOto;

    private String tndshhOto;

    private String nnXeMay;

    private String tndsbbXeMay;

    private String totalFeeBonus;

    public BigDecimal getTotalFee() {
        BigDecimal total = new BigDecimal(0);
        for (InsuranceDTO insurance: getInsurances()) {
            if (insurance.isIsEnable()) {
                total = total.add(insurance.getFee());
            }
        }
        return total;
    }

    public BigDecimal getVATFee() {
        return InsuranceFeeUtils.calculateAdditionalFee(this, getTotalFee());
    }

    public BigDecimal getFunTransferPayFee() {
        return InsuranceFeeUtils.calculateVnPayFee(getTotalFee(), PaymentType.FUND_TRANSFER);
    }

    public BigDecimal getAtmPayFee() {
        return InsuranceFeeUtils.calculateVnPayFee(getTotalFee(), PaymentType.ATM);
    }

    public BigDecimal getQrPayFee() {
        return InsuranceFeeUtils.calculateVnPayFee(getTotalFee(), PaymentType.QR_CODE);
    }

    public BigDecimal getVisaMasterPayFee() {
        return InsuranceFeeUtils.calculateVnPayFee(getTotalFee(), PaymentType.VISA_MASTER);
    }

}
