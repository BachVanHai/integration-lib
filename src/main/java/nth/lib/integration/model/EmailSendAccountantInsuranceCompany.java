package nth.lib.integration.model;

import nth.lib.integration.enumeration.ContractType;

public class EmailSendAccountantInsuranceCompany {

    private String greeting;
    private String contractCode;
    private String buyerName;
    private String insuranceCompany;
    private String insuranceCompanyShortName;
    private String insuranceType;
    private String totalFee;
    private String email;
    private String link;
    private String userId;
    private String sendRole;
    private ContractType contractType;

    public EmailSendAccountantInsuranceCompany() {
    }

    public String getInsuranceCompanyShortName() {
        return insuranceCompanyShortName;
    }

    public void setInsuranceCompanyShortName(String insuranceCompanyShortName) {
        this.insuranceCompanyShortName = insuranceCompanyShortName;
    }


    public String getSendRole() {
        return sendRole;
    }

    public void setSendRole(String sendRole) {
        this.sendRole = sendRole;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }
}
