package nth.lib.integration.model;

import nth.lib.integration.enumeration.DueDateType;

import java.math.BigDecimal;
import java.time.Instant;

public class InonDebtInfoModel {
    private Long accountId;
    private UsersDTO usersDTO;
    private BigDecimal currentDebt;
    private BigDecimal transactionLimit;
    private BigDecimal daylyLimit;
    private BigDecimal monthlyLimit;
    private BigDecimal yearlyLimit;
    private Instant dueDate;
    private BigDecimal totalDebt;
    private DueDateType dueDateType;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public UsersDTO getUsersDTO() {
        return usersDTO;
    }

    public void setUsersDTO(UsersDTO usersDTO) {
        this.usersDTO = usersDTO;
    }

    public BigDecimal getCurrentDebt() {
        return currentDebt;
    }

    public void setCurrentDebt(BigDecimal currentDebt) {
        this.currentDebt = currentDebt;
    }

    public BigDecimal getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(BigDecimal transactionLimit) {
        this.transactionLimit = transactionLimit;
    }

    public BigDecimal getDaylyLimit() {
        return daylyLimit;
    }

    public void setDaylyLimit(BigDecimal daylyLimit) {
        this.daylyLimit = daylyLimit;
    }

    public BigDecimal getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(BigDecimal monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public BigDecimal getYearlyLimit() {
        return yearlyLimit;
    }

    public void setYearlyLimit(BigDecimal yearlyLimit) {
        this.yearlyLimit = yearlyLimit;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(BigDecimal totalDebt) {
        this.totalDebt = totalDebt;
    }

    public DueDateType getDueDateType() {
        return dueDateType;
    }

    public void setDueDateType(DueDateType dueDateType) {
        this.dueDateType = dueDateType;
    }
}
