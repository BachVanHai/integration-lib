
package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
public class BSHCarInsuranceInfo {

    @JsonProperty("ma_dvi")
    private String maDvi;
    @JsonProperty("cb_ql")
    private String cbQl;
    @JsonProperty("email_cb_ql")
    private String emailCbQl;
    @JsonProperty("kenh_kt")
    private String kenhKt;
    @JsonProperty("cb_du")
    private String cbDu;
    @JsonProperty("ma_dl")
    private String maDl;
    @JsonProperty("so_id")
    private String soId;
    @JsonProperty("gcn_dt")
    private String gcnDt;
    @JsonProperty("gcn_m")
    private String gcnM;
    @JsonProperty("gcn_c")
    private String gcnC;
    @JsonProperty("gcn_s")
    private String gcnS;
    @JsonProperty("gcn_m_t")
    private String gcnMT;
    @JsonProperty("gcn_c_t")
    private String gcnCT;
    @JsonProperty("gcn_s_t")
    private String gcnST;
    @JsonProperty("ten_kh")
    private String tenKh;
    @JsonProperty("dchi_kh")
    private String dchiKh;
    @JsonProperty("dien_thoai_kh")
    private String dienThoaiKh;
    @JsonProperty("ten")
    private String ten;
    @JsonProperty("dchi")
    private String dchi;
    @JsonProperty("dien_thoai")
    private String dienThoai;
    @JsonProperty("gtinh")
    private String gtinh;
    @JsonProperty("email")
    private String email;
    @JsonProperty("nguoi_th")
    private String nguoiTh;
    @JsonProperty("dchi_th")
    private String dchiTh;
    @JsonProperty("bien_xe")
    private String bienXe;
    @JsonProperty("hang_xe")
    private String hangXe;
    @JsonProperty("hieu_xe")
    private String hieuXe;
    @JsonProperty("so_khung")
    private String soKhung;
    @JsonProperty("so_may")
    private String soMay;
    @JsonProperty("ma_bh")
    private String maBh;
    @JsonProperty("ttai")
    private String ttai;
    @JsonProperty("so_lpx")
    private String soLpx;
    @JsonProperty("so_cn")
    private String soCn;
    @JsonProperty("nam_sx")
    private String namSx;
    @JsonProperty("gia_xe")
    private String giaXe;
    @JsonProperty("nhom_xe")
    private String nhomXe;
    @JsonProperty("loai_xe")
    private String loaiXe;
    @JsonProperty("xuat_xu")
    private String xuatXu;
    @JsonProperty("gio_hl")
    private String gioHl;
    @JsonProperty("ngay_hl")
    private String ngayHl;
    @JsonProperty("gio_kt")
    private String gioKt;
    @JsonProperty("ngay_kt")
    private String ngayKt;
    @JsonProperty("ngay_cap")
    private String ngayCap;
    @JsonProperty("nd")
    private String nd;
    @JsonProperty("tienbb_ng")
    private String tienbbNg;
    @JsonProperty("tienbb_ts")
    private String tienbbTs;
    @JsonProperty("phibb_ds")
    private String phibbDs;
    @JsonProperty("tientn_ng")
    private String tientnNg;
    @JsonProperty("tientn_ts")
    private String tientnTs;
    @JsonProperty("tientn_hk")
    private String tientnHk;
    @JsonProperty("tientn_hh")
    private String tientnHh;
    @JsonProperty("phitn_ng")
    private String phitnNg;
    @JsonProperty("phitn_ts")
    private String phitnTs;
    @JsonProperty("phitn_hk")
    private String phitnHk;
    @JsonProperty("phitn_hh")
    private String phitnHh;
    @JsonProperty("tienbh_ntx")
    private String tienbhNtx;
    @JsonProperty("phibh_ntx")
    private String phibhNtx;
    @JsonProperty("tienbh_vc")
    private String tienbhVc;
    @JsonProperty("phibh_vc")
    private String phibhVc;
    @JsonProperty("tl_vc")
    private String tlVc;
    @JsonProperty("muc_mt")
    private String mucMt;
    @JsonProperty("DS_DKBS")
    private List<DSDKB> dSDKBS = null;

    @Data
    public static class DSDKB {
        @JsonProperty("dkbs_ma_dk")
        private String dkbsMaDk;
        @JsonProperty("dkbs_ma")
        private String dkbsMa;
        @JsonProperty("dkbs_nd")
        private String dkbsNd;
        @JsonProperty("dkbs_nt_tien")
        private String dkbsNtTien;
        @JsonProperty("dkbs_tien")
        private String dkbsTien;
        @JsonProperty("dkbs_pt")
        private String dkbsPt;
        @JsonProperty("dkbs_k_phi")
        private String dkbsKPhi;
        @JsonProperty("dkbs_phi")
        private String dkbsPhi;
        @JsonProperty("dkbs_han_muc")
        private String dkbsHanMuc;
    }

}
