package nth.lib.integration.model.mbbank;


import lombok.Data;

@Data
public class MBBankTransactionRes {

    private String id;

    private Merchant merchant;

    private String allowCard;

    private String cif;

    private Long amount;

    private String description;

    private Type type;

    private String successMessage;

    private String metadata;

    @Data
    public static class Merchant {
        private String code;
        private String name;
    }

    @Data
    public static class Type {
        private String code;
        private String name;
        private String allowCard;
    }
}
