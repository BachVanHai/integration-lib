package nth.lib.integration.model.mbbank;


import lombok.Data;

@Data
public class MBBankConfig {
    private Long id;

    private String rootUrl;

    private String merchantCode;

    private String merchantSecret;

    private String type;
}
