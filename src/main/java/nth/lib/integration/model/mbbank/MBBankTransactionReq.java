package nth.lib.integration.model.mbbank;


import lombok.Data;

@Data
public class MBBankTransactionReq {

    private String sessionId;

    private String allowCard;

    private String cif;

    private Long amount;

    private String description;

    private String type;

    private String successMessage;

    private String metadata;
}
