package nth.lib.integration.model.mbbank;


import lombok.Data;
import nth.lib.integration.enumeration.Gender;
import nth.lib.integration.model.UserAuthDTO;
import nth.lib.integration.model.UserDetailInfoDTO;
import nth.lib.integration.model.UsersDTO;

import java.time.Instant;
import java.util.UUID;

@Data
public class MBBankUserInfoRes {

    private String cif;

    private String sessionId;

    private String fullname;

    private Instant dob;

    private String idCardNo;

    private String mobile;

    private String email;

    public String getUsername() {
        return "MBBANK_" + this.cif;
    }

    public UsersDTO toUser() {
        UsersDTO usersDTO = new UsersDTO();
        UserDetailInfoDTO userDetailInfoDTO = new UserDetailInfoDTO();
        String userId = getUsername();
        usersDTO.setUsername(userId);
        usersDTO.setFullName(this.fullname);
        usersDTO.setUserType("KD");
        usersDTO.setPhoneNumber(this.mobile != null ? this.mobile :  userId);
        usersDTO.setEmail(this.email != null ? this.email :  this.cif + "@mbank.com");
        usersDTO.setGroupId("KHCN");
        usersDTO.setCreatedBy("admin");
        usersDTO.setGender(Gender.MALE.name());
        usersDTO.setCreatedDate(Instant.now());
        usersDTO.setIcType("CMND");
        usersDTO.setIcNumber(this.idCardNo);
        usersDTO.setLevel(1);
        userDetailInfoDTO.setAddress("");
        userDetailInfoDTO.setCompanyName("MBBank");
        userDetailInfoDTO.setCompanyShortName("MBB");
        userDetailInfoDTO.setCity("1");
        userDetailInfoDTO.setDistrict("1");
        userDetailInfoDTO.setWard("1");
        usersDTO.setUserDetails(userDetailInfoDTO);
        return usersDTO;
    }

    public UserAuthDTO toUserAuth() {
        UserAuthDTO userAuthDTO = new UserAuthDTO();
        userAuthDTO.setUsername(getUsername());
        userAuthDTO.setPassword(UUID.randomUUID().toString());
        userAuthDTO.setIsLocked(false);
        return userAuthDTO;
    }
}
