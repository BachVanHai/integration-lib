package nth.lib.integration.model.mbbank;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class MBBankUserInfoReq {
    private String token;
}
