package nth.lib.integration.model.mbbank;


import lombok.Data;

@Data
public class MBBankInitTransactionReq {

    private String contractId;

    private String contractCode;

    private String contractType;

    private Long amount;

    private String createdBy;

    private String sessionId;

    private String cif;
    private String type;
}
