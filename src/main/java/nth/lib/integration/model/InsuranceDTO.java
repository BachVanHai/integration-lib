package nth.lib.integration.model;

import nth.lib.integration.enumeration.Coverage;
import nth.lib.integration.enumeration.InsuranceCode;
import nth.lib.integration.enumeration.Unit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

public class InsuranceDTO implements Serializable {

    private Long id;

    private String productCode;

    private String packageCode;

    private InsuranceCode insuranceCode;

    private String description;


    private Boolean isEnable;

    private Integer duration;

    private Instant startValueDate;

    private Instant endValueDate;

    private BigDecimal value1;

    private Unit unit1;

    private BigDecimal feeRate1;

    private BigDecimal liability1;

    private BigDecimal count1;

    private BigDecimal value2;

    private Unit unit2;

    private BigDecimal feeRate2;


    private BigDecimal liability2;

    private BigDecimal count2;

    private BigDecimal value3;

    private Unit unit3;

    private BigDecimal feeRate3;

    private BigDecimal liability3;

    private BigDecimal count3;

    private UUID contractId;

    private BigDecimal feeMaterial;

    private BigDecimal feeAsset;

    private BigDecimal compensationLimitAsset;

    private Coverage coverageAsset;

    private BigDecimal compensationLimitMaterial;

    private Coverage coverageMaterial;

    public BigDecimal getFeeMaterial() {
        return feeMaterial;
    }

    public void setFeeMaterial(BigDecimal feeMaterial) {
        this.feeMaterial = feeMaterial;
    }

    public BigDecimal getFeeAsset() {
        return feeAsset;
    }

    public void setFeeAsset(BigDecimal feeAsset) {
        this.feeAsset = feeAsset;
    }

    public BigDecimal getCompensationLimitAsset() {
        return compensationLimitAsset;
    }

    public void setCompensationLimitAsset(BigDecimal compensationLimitAsset) {
        this.compensationLimitAsset = compensationLimitAsset;
    }

    public Coverage getCoverageAsset() {
        return coverageAsset;
    }

    public void setCoverageAsset(Coverage coverageAsset) {
        this.coverageAsset = coverageAsset;
    }

    public BigDecimal getCompensationLimitMaterial() {
        return compensationLimitMaterial;
    }

    public void setCompensationLimitMaterial(BigDecimal compensationLimitMaterial) {
        this.compensationLimitMaterial = compensationLimitMaterial;
    }

    public Coverage getCoverageMaterial() {
        return coverageMaterial;
    }

    public void setCoverageMaterial(Coverage coverageMaterial) {
        this.coverageMaterial = coverageMaterial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public InsuranceCode getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(InsuranceCode insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Instant getStartValueDate() {
        return startValueDate;
    }

    public void setStartValueDate(Instant startValueDate) {
        this.startValueDate = startValueDate;
    }

    public Instant getEndValueDate() {
        return endValueDate;
    }

    public void setEndValueDate(Instant endValueDate) {
        this.endValueDate = endValueDate;
    }

    public BigDecimal getValue1() {
        return value1;
    }

    public void setValue1(BigDecimal value1) {
        this.value1 = value1;
    }

    public Unit getUnit1() {
        return unit1;
    }

    public void setUnit1(Unit unit1) {
        this.unit1 = unit1;
    }

    public BigDecimal getFeeRate1() {
        return feeRate1;
    }

    public void setFeeRate1(BigDecimal feeRate1) {
        this.feeRate1 = feeRate1;
    }

    public BigDecimal getLiability1() {
        return liability1;
    }

    public void setLiability1(BigDecimal liability1) {
        this.liability1 = liability1;
    }

    public BigDecimal getCount1() {
        return count1;
    }

    public void setCount1(BigDecimal count1) {
        this.count1 = count1;
    }

    public BigDecimal getValue2() {
        return value2;
    }

    public void setValue2(BigDecimal value2) {
        this.value2 = value2;
    }

    public Unit getUnit2() {
        return unit2;
    }

    public void setUnit2(Unit unit2) {
        this.unit2 = unit2;
    }

    public BigDecimal getFeeRate2() {
        return feeRate2;
    }

    public void setFeeRate2(BigDecimal feeRate2) {
        this.feeRate2 = feeRate2;
    }

    public BigDecimal getLiability2() {
        return liability2;
    }

    public void setLiability2(BigDecimal liability2) {
        this.liability2 = liability2;
    }

    public BigDecimal getCount2() {
        return count2;
    }

    public void setCount2(BigDecimal count2) {
        this.count2 = count2;
    }

    public BigDecimal getValue3() {
        return value3;
    }

    public void setValue3(BigDecimal value3) {
        this.value3 = value3;
    }

    public Unit getUnit3() {
        return unit3;
    }

    public void setUnit3(Unit unit3) {
        this.unit3 = unit3;
    }

    public BigDecimal getFeeRate3() {
        return feeRate3;
    }

    public void setFeeRate3(BigDecimal feeRate3) {
        this.feeRate3 = feeRate3;
    }

    public BigDecimal getLiability3() {
        return liability3;
    }

    public void setLiability3(BigDecimal liability3) {
        this.liability3 = liability3;
    }

    public BigDecimal getCount3() {
        return count3;
    }

    public void setCount3(BigDecimal count3) {
        this.count3 = count3;
    }

    public UUID getContractId() {
        return contractId;
    }

    public void setContractId(UUID contractId) {
        this.contractId = contractId;
    }

    public BigDecimal getFee() {
        BigDecimal totalFee = new BigDecimal(0);
        if (this.getInsuranceCode() == InsuranceCode.CAR_TNDS_TN) {
            if (value1 != null) {
                totalFee = totalFee.add(value1);
            }
            if (value2 != null) {
                totalFee = totalFee.add(value2);
            }
            if (value3 != null) {
                totalFee = totalFee.add(value3);
            }
        } else {
            if (value1 != null) {
                totalFee = totalFee.add(value1);
            }
        }
        return totalFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InsuranceDTO)) {
            return false;
        }

        return id != null && id.equals(((InsuranceDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InsuranceDTO{" +
            "id=" + getId() +
            ", productCode='" + getProductCode() + "'" +
            ", packageCode='" + getPackageCode() + "'" +
            ", insuranceCode='" + getInsuranceCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", duration=" + getDuration() +
            ", startValueDate='" + getStartValueDate() + "'" +
            ", endValueDate='" + getEndValueDate() + "'" +
            ", value1=" + getValue1() +
            ", unit1='" + getUnit1() + "'" +
            ", feeRate1=" + getFeeRate1() +
            ", liability1=" + getLiability1() +
            ", count1=" + getCount1() +
            ", value2=" + getValue2() +
            ", unit2='" + getUnit2() + "'" +
            ", feeRate2=" + getFeeRate2() +
            ", liability2=" + getLiability2() +
            ", count2=" + getCount2() +
            ", value3=" + getValue3() +
            ", unit3='" + getUnit3() + "'" +
            ", feeRate3=" + getFeeRate3() +
            ", liability3=" + getLiability3() +
            ", count3=" + getCount3() +
            ", contractId=" + getContractId() +
            "}";
    }
}
