package nth.lib.integration.model;

import java.io.Serializable;

public class ApiDTO implements Serializable {

    private Long id;

    private String api;

    private Boolean isEnable;


    private Long partnerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApiDTO)) {
            return false;
        }

        return id != null && id.equals(((ApiDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApiDTO{" +
            "id=" + getId() +
            ", api='" + getApi() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", partnerId=" + getPartnerId() +
            "}";
    }
}
