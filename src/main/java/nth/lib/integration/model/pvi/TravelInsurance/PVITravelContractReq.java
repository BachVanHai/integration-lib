package nth.lib.integration.model.pvi.TravelInsurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import nth.lib.integration.enumeration.InsuranceCode;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Set;

public class PVITravelContractReq {

    final static DateTimeFormatter ddMMyyyy = DateTimeFormatter.ofPattern("dd/MM/yyyy")
            .withZone(ZoneId.systemDefault());
    final static DateTimeFormatter HHmm = DateTimeFormatter.ofPattern("HH:mm")
            .withZone(ZoneId.systemDefault());

    private String code;
    private String insuranceCode;
    private String journey;
    private String journeyInformation;
    private String insuranceAdditionalCode;
    private Boolean insuranceAdditionalCode2;
    private String customerPhone;
    private String customerName;
    private String customerEmail;
    private String customerAddress;
    private String customerIdentityNumber;
    private String customerBirthday;
    private String startInsurance;
    private String expiryDate;
    private String startTime;
    private String endTime;
    private String checksum;
    private Long insuranceMoney;
    private Long feePercentage;
    private Long insuranceTotalMoney;
    private String programCode;
    private Set<PeopleAttach> peopleAttaches;
    private Set<DetailInsurance> travelInsuranceCategory;

    @JsonProperty("journey_information")
    public String getJourneyInformation() {
        return journeyInformation;
    }

    public void setJourneyInformation(String journeyInformation) {
        switch (journeyInformation) {
            case "ROUND":{
                this.journeyInformation = "ROUND_TRIP";
                this.insuranceAdditionalCode = "030610";
                break;
            }
            case "ONEWAY":{
                this.journeyInformation = "ONE_WAY";
                this.insuranceAdditionalCode = "030610";
                break;
            }
            default:
                this.journeyInformation = "NONE";
        }
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("insurance_code")
    public String getInsuranceCode() {
        return insuranceCode;
    }

    @JsonProperty("customer_phone")
    public String getCustomerPhone() {
        return customerPhone;
    }

    @JsonProperty("customer_name")
    public String getCustomerName() {
        return customerName;
    }

    @JsonProperty("customer_email")
    public String getCustomerEmail() {
        return customerEmail;
    }

    @JsonProperty("customer_address")
    public String getCustomerAddress() {
        return customerAddress;
    }

    @JsonProperty("customer_identity_number")
    public String getCustomerIdentityNumber() {
        return customerIdentityNumber;
    }

    @JsonProperty("customer_birthday")
    public String getCustomerBirthday() {
        return customerBirthday;
    }

    @JsonProperty("start_insurance")
    public String getStartInsurance() {
        return startInsurance;
    }

    @JsonProperty("expiry_date")
    public String getExpiryDate() {
        return expiryDate;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("insurance_money")
    public Long getInsuranceMoney() {
        return insuranceMoney;
    }

    @JsonProperty("fee_percentage")
    public Long getFeePercentage() {
        return feePercentage;
    }

    @JsonProperty("insurance_total_money")
    public Long getInsuranceTotalMoney() {
        return insuranceTotalMoney;
    }

    @JsonProperty("list_attach_peoples")
    public Set<PeopleAttach> getPeopleAttaches() {
        return peopleAttaches;
    }

    @JsonProperty("journey")
    public String getJourney() {
        return journey;
    }

    @JsonProperty("insurance_additional_code")
    public String getInsuranceAdditionalCode() {
        return insuranceAdditionalCode;
    }

    @JsonProperty("insurance_additional_code_2")
    public Boolean getInsuranceAdditionalCode2() {
        return insuranceAdditionalCode2;
    }

    @JsonProperty("travel_insurance_category")
    public Set<DetailInsurance> getTravelInsuranceCategory() {
        return travelInsuranceCategory;
    }

    @JsonProperty("check_sum")
    public String getCheckSum() {
        return checksum;
    }

    @JsonProperty("program_code")
    public String getProgramCode() {
        return programCode;
    }

    public void setJourney(String journey) {
        this.journey = journey;
    }

    public void setTravelInsuranceCategory(Set<DetailInsurance> travelInsuranceCategory) {
        this.travelInsuranceCategory = travelInsuranceCategory;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }


    public void setCode(String code) {
        this.code = code;
    }

    public void setInsuranceCode(String insuranceCode) {
        if (insuranceCode.equals(InsuranceCode.TA.name())) {
            this.insuranceCode = "010604";
        } else if (insuranceCode.equals(InsuranceCode.TD.name())) {
            this.insuranceCode = "010601";
        }
    }

    public void setInsuranceAdditionalCode2(Boolean insuranceAdditionalCode2) {
        this.insuranceAdditionalCode2 = insuranceAdditionalCode2;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public void setCustomerIdentityNumber(String customerIdentityNumber) {
        this.customerIdentityNumber = customerIdentityNumber;
    }

    public void setCustomerBirthday(Instant customerBirthday) {
        this.customerBirthday = ddMMyyyy.format(customerBirthday);
    }

    public void setStartInsurance(Instant startInsurance) {
        this.startInsurance = ddMMyyyy.format(startInsurance);
    }

    public void setExpiryDate(Instant expiryDate) {
        this.expiryDate = ddMMyyyy.format(expiryDate);
    }

    public void setStartTime(Instant startTime) {
        this.startTime = HHmm.format(startTime);
    }

    public void setEndTime(Instant endTime) {
        this.endTime = HHmm.format(endTime);
    }

    public void setInsuranceMoney(Long insuranceMoney) {
        this.insuranceMoney = insuranceMoney;
    }

    public void setFeePercentage(Long feePercentage) {
        this.feePercentage = feePercentage;
    }

    public void setInsuranceTotalMoney(Long insuranceTotalMoney) {
        this.insuranceTotalMoney = insuranceTotalMoney;
    }

    public void setPeopleAttaches(Set<PeopleAttach> peopleAttaches) {
        this.peopleAttaches = peopleAttaches;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public static class PeopleAttach {
        private String name;
        private String gender;
        private String birthday;
        private String address;
        private String phone;
        private String identityNumber;

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("gender")
        public String getGender() {
            if (this.gender.equals("MALE")) {
                return "NAM";
            } else if (this.gender.equals("FEMALE")){
                return "NU";
            } else {
                return "KHAC";
            }
        }

        @JsonProperty("birthday")
        public String getBirthday() {
            return birthday;
        }

        @JsonProperty("address")
        public String getAddress() {
            return address;
        }

        @JsonProperty("phone")
        public String getPhone() {
            return phone;
        }

        @JsonProperty("identity_number")
        public String getIdentityNumber() {
            return identityNumber;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public void setBirthday(Instant birthday) {
            this.birthday = ddMMyyyy.format(birthday);
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setIdentityNumber(String identityNumber) {
            this.identityNumber = identityNumber;
        }
    }

    public static class DetailInsurance {

        @JsonValue
        public Builder builder;

        public DetailInsurance(Builder builder) {
            this.builder = builder;
        }

        public static class Builder {
            private BigDecimal insuranceMoney;
            private BigDecimal feePercentage;
            private BigDecimal insuranceTotalMoney;
            private String code;

            @JsonProperty("insurance_money")
            public BigDecimal getInsuranceMoney() {
                return insuranceMoney;
            }

            @JsonProperty("fee_percentage")
            public BigDecimal getFeePercentage() {
                return feePercentage;
            }

            @JsonProperty("insurance_total_money")
            public BigDecimal getInsuranceTotalMoney() {
                return insuranceTotalMoney;
            }

            @JsonProperty("code")
            public String getCode() {
                return code;
            }

            public Builder setInsuranceMoney(BigDecimal insuranceMoney) {
                this.insuranceMoney = insuranceMoney;
                return this;
            }

            public Builder setFeePercentage(BigDecimal feePercentage) {
                this.feePercentage = feePercentage;
                return this;
            }

            public Builder setInsuranceTotalMoney(BigDecimal insuranceTotalMoney) {
                this.insuranceTotalMoney = insuranceTotalMoney;
                return this;
            }

            public Builder setCode(String code) {
                this.code = code;
                return this;
            }

            public DetailInsurance build(){
                return new DetailInsurance(this);
            }
        }
    }
}
