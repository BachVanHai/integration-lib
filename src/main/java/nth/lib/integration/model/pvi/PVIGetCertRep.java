package nth.lib.integration.model.pvi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PVIGetCertRep {
    @JsonProperty("Status")
    private String status;

    @JsonProperty("Message")
    private String massage;

    @JsonProperty("CardId")
    private String cardId;

    @JsonProperty("SerialNumber")
    private String serialNumber;

    @JsonProperty("RequestId")
    private String requestId;

    @JsonProperty("PolicyNumber")
    private String policyNumber;

    @JsonProperty("URL")
    private String url;

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Message")
    public void setMassage(String massage) {
        this.massage = massage;
    }

    @JsonProperty("CardId")
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @JsonProperty("SerialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("RequestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("PolicyNumber")
    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @JsonProperty("URL")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public String getMassage() {
        return massage;
    }

    public String getCardId() {
        return cardId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getUrl() {
        return url;
    }
}
