package nth.lib.integration.model.pvi.TravelInsurance;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PVITravelContractRep {

    private String status;
    private String message;
    private String prKey;
    private String code;
    private String accessToken;
    private String contractId;
    private Long insurCompCertId;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Long getInsurCompCertId() {
        return insurCompCertId;
    }

    public void setInsurCompCertId(Long insurCompCertId) {
        this.insurCompCertId = insurCompCertId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("Pr_key")
    public void setPrKey(String prKey) {
        this.prKey = prKey;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getPrKey() {
        return prKey;
    }
}
