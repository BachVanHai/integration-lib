package nth.lib.integration.model.pvi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

@Data
public class PVILoginReq {
    @JsonProperty("email")
    private String email;

    @JsonProperty("password")
    private String password;

    public static void main(String[] args) {
        int hour = Instant.now().plus(1, ChronoUnit.HOURS).atOffset(ZoneOffset.ofHours(7)).getHour();
        Instant nowDateTime = Instant.now().atZone(ZoneOffset.ofHours(0)).withHour(hour).withMinute(0).withSecond(0).withNano(0).toInstant();

        System.out.println(nowDateTime.toString());
    }

}
