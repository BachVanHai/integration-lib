package nth.lib.integration.model.notification;

import lombok.Data;

@Data
public class ExpiredVehicleContract {
    private String greeting;
    private String contractCode;
    private String customerName;
    private String insuranceType;
    private String insuranceCompany;
    private String startValueDate;
    private String endValueDate;
    private String totalFee;
    private String contractStatus;
    private String code;
    private String contractId;
    private String linkRenewalContract;
    private String templateDescription;
}
