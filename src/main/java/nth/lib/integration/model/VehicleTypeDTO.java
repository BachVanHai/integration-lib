package nth.lib.integration.model;

import nth.lib.integration.enumeration.CapacityType;
import nth.lib.integration.enumeration.Usage;
import nth.lib.integration.enumeration.VehicleType;

import java.io.Serializable;

public class VehicleTypeDTO implements Serializable {

    private Long id;

    private String code;

    private String carType;

    private VehicleType type;

    private String name;

    private String nameLanguageId;

    private Boolean isEnable;

    private Integer orderIdx;

    private CapacityType capacityType;

    private Usage businessStatus;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameLanguageId() {
        return nameLanguageId;
    }

    public void setNameLanguageId(String nameLanguageId) {
        this.nameLanguageId = nameLanguageId;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Integer getOrderIdx() {
        return orderIdx;
    }

    public void setOrderIdx(Integer orderIdx) {
        this.orderIdx = orderIdx;
    }

    public CapacityType getCapacityType() {
        return capacityType;
    }

    public void setCapacityType(CapacityType capacityType) {
        this.capacityType = capacityType;
    }

    public Usage getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(Usage businessStatus) {
        this.businessStatus = businessStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VehicleTypeDTO)) {
            return false;
        }

        return id != null && id.equals(((VehicleTypeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleTypeDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", carType='" + getCarType() + "'" +
            ", type='" + getType() + "'" +
            ", name='" + getName() + "'" +
            ", nameLanguageId='" + getNameLanguageId() + "'" +
            ", isEnable='" + isIsEnable() + "'" +
            ", orderIdx=" + getOrderIdx() +
            ", capacityType='" + getCapacityType() + "'" +
            ", businessStatus='" + getBusinessStatus() + "'" +
            "}";
    }
}
