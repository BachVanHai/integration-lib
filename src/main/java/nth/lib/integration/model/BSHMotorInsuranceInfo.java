
package nth.lib.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ma_dvi",
        "cb_ql",
        "email_cb_ql",
        "kenh_kt",
        "cb_du",
        "ma_dl",
        "gcn_m",
        "gcn_c",
        "gcn_s",
        "ten",
        "dchi",
        "dien_thoai",
        "email",
        "bien_xe",
        "hang_xe",
        "hieu_xe",
        "so_khung",
        "so_may",
        "ma_bh",
        "so_cn",
        "gio_hl",
        "ngay_hl",
        "gio_kt",
        "ngay_kt",
        "ngay_cap",
        "nd",
        "tienbh_ng",
        "tienbh_ts",
        "phibh_ds",
        "tienbh_nn",
        "phibh_nn"
})
@Data
public class BSHMotorInsuranceInfo {

    @JsonProperty("ma_dvi")
    private String maDvi;
    @JsonProperty("cb_ql")
    private String cbQl;
    @JsonProperty("email_cb_ql")
    private String emailCbQl;
    @JsonProperty("kenh_kt")
    private String kenhKt;
    @JsonProperty("cb_du")
    private String cbDu;
    @JsonProperty("ma_dl")
    private String maDl;
    @JsonProperty("gcn_m")
    private String gcnM;
    @JsonProperty("gcn_c")
    private String gcnC;
    @JsonProperty("gcn_s")
    private String gcnS;
    @JsonProperty("ten")
    private String ten;
    @JsonProperty("dchi")
    private String dchi;
    @JsonProperty("dien_thoai")
    private String dienThoai;
    @JsonProperty("email")
    private String email;
    @JsonProperty("bien_xe")
    private String bienXe;
    @JsonProperty("hang_xe")
    private String hangXe;
    @JsonProperty("hieu_xe")
    private String hieuXe;
    @JsonProperty("so_khung")
    private String soKhung;
    @JsonProperty("so_may")
    private String soMay;
    @JsonProperty("ma_bh")
    private String maBh;
    @JsonProperty("so_cn")
    private String soCn;
    @JsonProperty("gio_hl")
    private String gioHl;
    @JsonProperty("ngay_hl")
    private String ngayHl;
    @JsonProperty("gio_kt")
    private String gioKt;
    @JsonProperty("ngay_kt")
    private String ngayKt;
    @JsonProperty("ngay_cap")
    private String ngayCap;
    @JsonProperty("nd")
    private String nd;
    @JsonProperty("tienbh_ng")
    private String tienbhNg;
    @JsonProperty("tienbh_ts")
    private String tienbhTs;
    @JsonProperty("phibh_ds")
    private String phibhDs;
    @JsonProperty("tienbh_nn")
    private String tienbhNn;
    @JsonProperty("phibh_nn")
    private String phibhNn;

}
