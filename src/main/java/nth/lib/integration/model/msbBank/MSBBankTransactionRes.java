package nth.lib.integration.model.msbBank;

import lombok.Data;

@Data
public class MSBBankTransactionRes {
    String code;
    String desc;
    ResponseBody data;
    String image;

    @Data
    public static class ResponseBody {
        String id;
        String transDate;
        String tranId;
        String amount;
        String billNumber;
        String currency;
        String merchantName;
        String mId;
        String terminalId;
        String secureHash;
        String status;
    }
}
