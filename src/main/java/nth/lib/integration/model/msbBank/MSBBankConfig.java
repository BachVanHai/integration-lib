package nth.lib.integration.model.msbBank;


import lombok.Data;

@Data
public class MSBBankConfig {

    private Long id;

    private String rootUrl;

    private String merchantName;

    private String secretId;

    private String checkSumSecret;

    private String encryptSecretKey;

    private String accessCode;

    private String mId;

    private String tid;

}
