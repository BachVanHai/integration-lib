package nth.lib.integration.model.msbBank;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class MSBBankTransactionReq {

    private String transDate;
    private String transId;
    private String merchantName;
    @JsonProperty("mId")
    private String mId;
    @JsonProperty("tId")
    private String tId;
    private Long amount;
    private String billNumber;
    private String currency;
    private String secureHash;

}
