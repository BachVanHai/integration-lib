package nth.lib.integration.model.msbBank;

import lombok.Data;

import java.time.Instant;

@Data
public class MSBBankInitTransactionReq {

    private String contractId;

    private String billNumber;

    private Long amount;

    private Instant transDate;

    private String contractType;

    private String transactionCode;


}
