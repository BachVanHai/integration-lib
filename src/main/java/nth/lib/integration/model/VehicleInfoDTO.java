package nth.lib.integration.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleInfoDTO implements Serializable {
    private String numberPlate;
    private String fullName;
    private String address;
    private String phone;
    private String taxCode;
    private String manufactureName;
    private String brandName;
    private String city;
    private String vehicleType;
}
