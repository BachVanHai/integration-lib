package nth.lib.integration.model;

import lombok.Data;
import nth.lib.integration.enumeration.ContractType;

import java.io.Serializable;

@Data
public class ReasonForRejectDTO implements Serializable {

    private String emailSaler;
    private String salerId;
    private String saleName;
    private String emailOwner;
    private String ownerId;
    private String ownerName;
    private String contractCode;
    private String reasonForReject;
    private ContractType contractType;
    private String numberPlate;
    private String machineNo;
    private String frameNo;


}
