package nth.lib.integration.model;

import lombok.Data;

@Data
public class DebtContractInfo {
    String createdDate;
    String contractCode;
    String certNo;
    String amount;
    String dueDate;
    String customerName;
}
