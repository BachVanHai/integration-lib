package nth.lib.integration.model;

import lombok.Data;

import java.time.Instant;

@Data
public class DeviceLogDTO {

    private String sessionValue;

    private Instant loginTime;

    private Instant logoutTime;

    private Instant lastActiveTime;

    private String longitude;

    private String latitude;

    private String userId;

    private String deviceId;

    private Boolean isEnabled;

    private Boolean isDeleted;

    private String ipClient;

    private String appCode;

    private String appVersion;

    private String deviceFirebaseToken;

    private String model;

    private String os;

    private String macAddress;

    private String userAgent;
}
