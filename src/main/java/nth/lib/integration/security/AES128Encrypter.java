package nth.lib.integration.security;

import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Program to Encrypt/Decrypt String Using AES 128 bit (16 byte) Encryption Algorithm
 */
public class AES128Encrypter {

    private static final String CIPHER_TRANSFORM = "AES/CBC/PKCS5PADDING";
    private static final String AES_ENCRYPTION_ALGORITHM = "AES";

    private String encryptionKey;

    private AES128Encrypter(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public static AES128Encrypter getInstance() {
        return new AES128Encrypter("MO0sbcr7DKYVDYd4");
    }

    /**
     * Method for Encrypt Plain String Data
     *
     * @param plainText
     * @return encryptedText
     */
    public String encrypt(String plainText) throws Exception {
        if (StringUtils.isEmpty(encryptionKey)) {
            throw new Exception("EncryptionKey is null");
        }
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORM);
        byte[] key = encryptionKey.getBytes(StandardCharsets.UTF_8.name());
        SecretKeySpec secretKey = new SecretKeySpec(key, AES_ENCRYPTION_ALGORITHM);
        IvParameterSpec ivparameterspec = new IvParameterSpec(key);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivparameterspec);
        byte[] cipherText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(cipherText);
    }

    /**
     * Method For Get encryptedText and Decrypted provided String
     *
     * @param encryptedText
     * @return decryptedText
     */
    public String decrypt(String encryptedText) throws Exception {
        if (StringUtils.isEmpty(encryptionKey)) {
            throw new Exception("EncryptionKey is null");
        }
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORM);
        byte[] key = encryptionKey.getBytes(StandardCharsets.UTF_8.name());
        SecretKeySpec secretKey = new SecretKeySpec(key, AES_ENCRYPTION_ALGORITHM);
        IvParameterSpec ivparameterspec = new IvParameterSpec(key);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivparameterspec);
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] cipherText = decoder.decode(encryptedText.getBytes(StandardCharsets.UTF_8));
        return new String(cipher.doFinal(cipherText), StandardCharsets.UTF_8.name());
    }

}
